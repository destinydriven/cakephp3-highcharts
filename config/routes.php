<?php

use Cake\Routing\Router;

Router::plugin('Highcharts', function ($routes) {
        $routes->fallbacks('InflectedRoute');
});
