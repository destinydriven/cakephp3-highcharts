<?php
/**
 *  CakePHP Highcharts Plugin
 *
 *  Copyright (C) 2015 Kurn La Montagne / destinydriven
 *  <https://github.com/destinydriven>
 *
 *  Multi-licensed under:
 *      MPL <http://www.mozilla.org/MPL/MPL-1.1.html>
 *      LGPL <http://www.gnu.org/licenses/lgpl.html>
 *      GPL <http://www.gnu.org/licenses/gpl.html>
 *      Apache License, Version 2.0 <http://www.apache.org/licenses/LICENSE-2.0.html>
 */
namespace Highcharts\Lib;

use Ghunti\HighchartsPHP\HighchartOption;

class HighchartsSeriesData {

        public $name;
        public $type;
        public $data = array();

        public function addName($name) {
                $this->name = $name;
                return $this;
        }

        public function addType($type) {
                $this->type = $type;
                return $this;
        }

        public function addData($data) {
                $this->data = $data;
                return $this;
        }

}
