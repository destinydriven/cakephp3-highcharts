<?php
/**
 *  CakePHP Highcharts Plugin
 * 
 * 	Copyright (C) 2015 Kurn La Montagne / destinydriven
 * 	<https://github.com/destinydriven> 
 * 
 * 	Multi-licensed under:
 * 		MPL <http://www.mozilla.org/MPL/MPL-1.1.html>
 * 		LGPL <http://www.gnu.org/licenses/lgpl.html>
 * 		GPL <http://www.gnu.org/licenses/gpl.html>
 * 		Apache License, Version 2.0 <http://www.apache.org/licenses/LICENSE-2.0.html>
 */
namespace Highcharts\View\Helper;

use Cake\View\Helper;
use Cake\View\View;
use Cake\Utility\Inflector;
use Cake\View\Helper\SessionHelper;
use Cake\Event\Event;
use Ghunti\HighchartsPHP\Highchart;
use Ghunti\HighchartsPHP\HighchartOption;
use Ghunti\HighchartsPHP\HighchartOptionRenderer;

class HighchartsHelper extends Helper {

        public $helpers = array('Html', 'Session');
        public $charts = null;
        public $chart_name = '';
        public $chartInstance = null;
        public $session;

/**
 * Constructor
 * 
 * @param View
 * @param $config array
 */
        public function __construct(View $View, $config = array()) {
                parent::__construct($View, $config);
                $this->chartInstance = new Highchart();
                $this->charts = $this->_getCharts();                
        }

        
        public function afterRender(Event $event = null, $viewFile = null) {
               // $this->session->delete('Highcharts.Charts');
        }

        public function beforeLayout( Event $event = null, $layoutFile = null) {
                $js = $this->chartInstance->getScripts();
                $this->Html->script( $js, array('plugin' => 'Highcharts', 'block' => true));                
                return true;
        }

        protected function _getCharts() {
                static $read = false;
                if ($read === true) {
                        return $this->charts;
                } else {
                        $this->session = $this->request->session();
                        $this->charts = $this->session->read('Highcharts.Charts');
                        $read = true;
                        return $this->charts;
                }
        }        

        public function includeExtraScripts() {
              return $this->chartInstance->includeExtraScripts();
        }

        public function render($chartInstance, $name = null) {
                $safeName = Inflector::camelize($name); 
                // render with no script tags so we can use Cake to do it
                $out = $chartInstance->render($safeName, null, false);  

                return $this->Html->scriptBlock($out, array('block' => false, 'defer' => true));
        }

}
