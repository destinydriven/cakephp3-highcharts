<?php
/**
 *  CakePHP Highcharts Plugin
 *
 * 	Copyright (C) 2014 Kurn La Montagne / destinydriven
 * 	<https://github.com/destinydriven>
 *
 * 	Multi-licensed under:
 * 		MPL <http://www.mozilla.org/MPL/MPL-1.1.html>
 * 		LGPL <http://www.gnu.org/licenses/lgpl.html>
 * 		GPL <http://www.gnu.org/licenses/gpl.html>
 * 		Apache License, Version 2.0 <http://www.apache.org/licenses/LICENSE-2.0.html>
 */
namespace Highcharts\Controller;

use Highcharts\Controller\AppController;

class MultiSeriesDemoController extends AppController {

        public $name = 'MultiSeriesDemo';
        public $helpers = array('Html');
        public $uses = array();
        public $layout = 'Highcharts.demo';
        public $Highcharts = null;
        public $chartData1 = array(7.0, 6.9, 9.5, 14.5, 18.2, 21.5, 25.2, 26.5, 23.3, 18.3, 13.9, 9.6);
        public $chartData2 = array(0.2, 0.9, 5.5, 11.5, 17.2, 22.5, 24.2, 21.5, 23.3, 14.3, 18.9, 2.6);
        public $chartData3 = array(0.9, 0.6, 3.5, 8.5, 13.2, 17.5, 18.2, 17.5, 14.3, 12.3, 8.9, 5.6);
        
        public function initialize() {
                parent::initialize();
                $this->loadComponent('Highcharts.Highcharts');
        }

        public function area() {
                
                $chartName = 'Area Chart';

                $myChart = $this->Highcharts->createChart();

                $myChart->chart->renderTo = 'areawrapper';
                $myChart->chart->type = 'area';
                $myChart->chart->width = 800;
                $myChart->chart->height = 600;
                $myChart->chart->marginTop = 60;
                $myChart->chart->marginLeft = 90;
                $myChart->chart->marginRight = 30;
                $myChart->chart->marginBottom = 110;
                $myChart->chart->spacingRight = 10;
                $myChart->chart->spacingBottom = 15;
                $myChart->chart->spacingLeft = 0;
                $myChart->chart->alignTicks = false;
                $myChart->chart->backgroundColor->linearGradient = array(0, 0, 0, 300);
                $myChart->chart->backgroundColor->stops =  array(array(0, 'rgb(217, 217, 217)'), array(1, 'rgb(255, 255, 255)'));
                $myChart->title->text = 'Monthly Sales Summary';
                $myChart->title->style->color = '#0099ff';
                $myChart->title->x = 20;
                $myChart->title->y = 20;
                $myChart->title->align = 'left';
                $myChart->title->floating  = true;
                $myChart->title->style->font = '18px Metrophobic, Arial, sans-serif';                
                $myChart->legend->enabled  = true;
                $myChart->legend->layout  = 'horizontal';
                $myChart->legend->align = 'center';
                $myChart->legend->vertical->align = 'bottom';
                $myChart->legend->item->style  = array('color' => '#222');
                $myChart->legend->backgroundColor->linearGradient = array(0, 0, 0, 25);
                $myChart->legend->backgroundColor->stops = array(array(0, 'rgb(217, 217, 217)'), array(1, 'rgb(255, 255, 255)'));
                $myChart->tooltip->enabled = false;
                $myChart->xAxis->labels->enabled = true;
                $myChart->xAxis->labels->align = 'right';
                $myChart->xAxis->labels->step = 1;
                $myChart->xAxis->labels->rotation = -35;
                $myChart->xAxis->labels->x = 5;
                $myChart->xAxis->labels->y = 20;
                $myChart->xAxis->categories = array(
                        'Jan',
                        'Feb',
                        'Mar',
                        'Apr',
                        'May',
                        'Jun',
                        'Jul',
                        'Aug',
                        'Sep',
                        'Oct',
                        'Nov',
                        'Dec'
                );
                $myChart->yAxis->title->text = 'Units Sold';
                $myChart->credits->enabled = false;
                $myChart->credits->text  = 'Example.com';
                $myChart->credits->url = 'http://example.com';
                        
                $myChart->series[] = array(
                    'name' => 'Tokyo',
                    'data' => $this->chartData1
                );
                
                $myChart->series[] = array(
                    'name' => 'London',
                    'data' => $this->chartData2
                );
                
                $myChart->series[] = array(
                    'name' => 'New York',
                    'data' => $this->chartData3
                );
                
                $this->set(compact('myChart', 'chartName'));
        }

        public function areaspline() {
                
                $chartName = 'Area Spline Chart';

                $myChart = $this->Highcharts->createChart();
                $myChart->chart->type = 'areaspline';
                $myChart->chart->renderTo = 'areasplinewrapper';
                $myChart->chart->width = 800;
                $myChart->chart->height = 600;
                $myChart->chart->marginTop = 60;
                $myChart->chart->marginLeft = 90;
                $myChart->chart->marginRight = 30;
                $myChart->chart->marginBottom = 110;
                $myChart->chart->spacingRight = 10;
                $myChart->chart->spacingBottom = 15;
                $myChart->chart->spacingLeft = 0;
                $myChart->chart->alignTick = FALSE;
                $myChart->chart->backgroundColor->linearGradient = array(0, 0, 0, 300);
                $myChart->chart->backgroundColor->stops = array(array(0, 'rgb(217, 217, 217)'), array(1, 'rgb(255, 255, 255)'));
                $myChart->title->text = 'Monthly Sales Summary';
                $myChart->title->align = 'left';
                $myChart->title->foating = TRUE;
                $myChart->title->styleFont = '18px Metrophobic, Arial, sans-serif';
                $myChart->title->styleColor = '#0099ff';
                $myChart->title->x = 20;
                $myChart->title->y = 20;
                $myChart->legend->enabled = TRUE;
                $myChart->legend->layout = 'horizontal';
                $myChart->legend->align = 'center';
                $myChart->legend->verticalAlign  = 'bottom';
                $myChart->legend->itemStyle = array('color' => '#222');
                $myChart->legend->backgroundColor->linearGradient = array(0, 0, 0, 25);
                $myChart->legend->backgroundColor->stops = array(array(0, 'rgb(217, 217, 217)'), array(1, 'rgb(255, 255, 255)'));
                $myChart->tooltip->enabled = FALSE;
                $myChart->exporting->enabled = FALSE;
                $myChart->xAxis->labels->enabled = TRUE;
                $myChart->xAxis->labels->align = 'right';
                $myChart->xAxis->labels->step = 1;
                $myChart->xAxis->labels->x = 5;
                $myChart->xAxis->labels->y = 20;
                $myChart->xAxis->categories = array(
                        'Jan',
                        'Feb',
                        'Mar',
                        'Apr',
                        'May',
                        'Jun',
                        'Jul',
                        'Aug',
                        'Sep',
                        'Oct',
                        'Nov',
                        'Dec'
                    );
                $myChart->yAxis->title->text = 'Units Sold';

                $myChart->series[] = array(
                    'name' => 'Tokyo',
                    'data' => $this->chartData1
                ); 
                
                $myChart->series[] = array(
                    'name' => 'London',
                    'data' => $this->chartData2
                );
                
                $myChart->series[] = array(
                    'name' => 'New york',
                    'data' => $this->chartData3
                );  
                
                $this->set(compact('myChart','chartName'));
        }

        public function bar() {

                $chartName = 'Bar Chart';

                $myChart = $this->Highcharts->createChart();

                $myChart->chart->renderTo = "container";
                $myChart->chart->type = "bar";
                $myChart->title->text = "Historic World Population by Region";
                $myChart->subtitle->text = "Source: Wikipedia.org";
                $myChart->xAxis->categories = array(
                    'Africa',
                    'America',
                    'Asia',
                    'Europe',
                    'Oceania'
                );
                $myChart->xAxis->title->text = null;
                $myChart->yAxis->min = 0;
                $myChart->yAxis->title->text = "Population (millions)";
                $myChart->yAxis->title->align = "high";

                $myChart->tooltip->formatter = $this->Highcharts->createJsExpr( "function() {
                    return '' + this.series.name +': '+ this.y +' millions';}");

                $myChart->plotOptions->bar->dataLabels->enabled = 1;
                $myChart->legend->layout = "vertical";
                $myChart->legend->align = "right";
                $myChart->legend->verticalAlign = "top";
                $myChart->legend->x = - 100;
                $myChart->legend->y = 100;
                $myChart->legend->floating = 1;
                $myChart->legend->borderWidth = 1;
                $myChart->legend->backgroundColor = "#FFFFFF";
                $myChart->legend->shadow = 1;
                $myChart->credits->enabled = false;

                $myChart->series[] = array(
                    'name' => "Year 1800",
                    'data' => array(
                        107,
                        31,
                        635,
                        203,
                        2
                    )
                );

                $myChart->series[] = array(
                    'name' => "Year 1900",
                    'data' => array(
                        133,
                        156,
                        947,
                        408,
                        6
                    )
                );

                $myChart->series[] = array(
                    'name' => "Year 2008",
                    'data' => array(
                        973,
                        914,
                        4054,
                        732,
                        34
                    )
                );

                $this->set(compact('myChart', 'chartName'));
                
        }

        public function column() {
                
                $chartName = 'Column Chart';

                $myChart = $this->Highcharts->createChart();
                
                $myChart->chart->renderTo = "container";
                $myChart->chart->type = "column";
                $myChart->title->text = "Monthly Average Rainfall";
                $myChart->subtitle->text = "Source: WorldClimate.com";

                $myChart->xAxis->categories = array(
                    "Jan",
                    "Feb",
                    "Mar",
                    "Apr",
                    "May",
                    "Jun",
                    "Jul",
                    "Aug",
                    "Sep",
                    "Oct",
                    "Nov",
                    "Dec"
                );

                $myChart->yAxis->min = 0;
                $myChart->yAxis->title->text = "Rainfall (mm)";
                $myChart->legend->layout = "vertical";
                $myChart->legend->backgroundColor = "#FFFFFF";
                $myChart->legend->align = "left";
                $myChart->legend->verticalAlign = "top";
                $myChart->legend->x = 100;
                $myChart->legend->y = 70;
                $myChart->legend->floating = 1;
                $myChart->legend->shadow = 1;

                $myChart->tooltip->formatter = $this->Highcharts->createJsExpr("function() {
                    return '' + this.x +': '+ this.y +' mm';}");

                $myChart->plotOptions->column->pointPadding = 0.2;
                $myChart->plotOptions->column->borderWidth = 0;

                $myChart->series[] = array(
                    'name' => "Tokyo",
                    'data' => array(
                        49.9,
                        71.5,
                        106.4,
                        129.2,
                        144.0,
                        176.0,
                        135.6,
                        148.5,
                        216.4,
                        194.1,
                        95.6,
                        54.4
                    )
                );

                $myChart->series[] = array(
                    'name' => "New York",
                    'data' => array(
                        83.6,
                        78.8,
                        98.5,
                        93.4,
                        106.0,
                        84.5,
                        105.0,
                        104.3,
                        91.2,
                        83.5,
                        106.6,
                        92.3
                    )
                );

                $myChart->series[] = array(
                    'name' => "London",
                    'data' => array(
                        48.9,
                        38.8,
                        39.3,
                        41.4,
                        47.0,
                        48.3,
                        59.0,
                        59.6,
                        52.4,
                        65.2,
                        59.3,
                        51.2
                    )
                );

                $myChart->series[] = array(
                    'name' => "Berlin",
                    'data' => array(
                        42.4,
                        33.2,
                        34.5,
                        39.7,
                        52.6,
                        75.5,
                        57.4,
                        60.4,
                        47.6,
                        39.1,
                        46.8,
                        51.1
                    )
                );

                $this->set(compact('myChart', 'chartName'));
        }

        public function line() {
                
                $chartName = 'Line Chart';

                $myChart = $this->Highcharts->createChart();
                
                $myChart->chart = array(
                    'renderTo' => 'container',
                    'type' => 'line',
                    'marginRight' => 130,
                    'marginBottom' => 25
                );

                $myChart->title = array(
                    'text' => 'Monthly Average Temperature',
                    'x' => - 20
                );
                
                $myChart->subtitle = array(
                    'text' => 'Source: WorldClimate.com',
                    'x' => - 20
                );

                $myChart->xAxis->categories = array(
                    'Jan',
                    'Feb',
                    'Mar',
                    'Apr',
                    'May',
                    'Jun',
                    'Jul',
                    'Aug',
                    'Sep',
                    'Oct',
                    'Nov',
                    'Dec'
                );

                $myChart->yAxis = array(
                    'title' => array(
                        'text' => 'Temperature (°C)'
                    ),
                    'plotLines' => array(
                        array(
                            'value' => 0,
                            'width' => 1,
                            'color' => '#808080'
                        )
                    )
                );
                $myChart->legend = array(
                    'layout' => 'vertical',
                    'align' => 'right',
                    'verticalAlign' => 'top',
                    'x' => - 10,
                    'y' => 100,
                    'borderWidth' => 0
                );

                $myChart->series[] = array(
                    'name' => 'Tokyo',
                    'data' => array(
                        7.0,
                        6.9,
                        9.5,
                        14.5,
                        18.2,
                        21.5,
                        25.2,
                        26.5,
                        23.3,
                        18.3,
                        13.9,
                        9.6
                    )
                );
                
                $myChart->series[] = array(
                    'name' => 'New York',
                    'data' => array(
                        - 0.2,
                        0.8,
                        5.7,
                        11.3,
                        17.0,
                        22.0,
                        24.8,
                        24.1,
                        20.1,
                        14.1,
                        8.6,
                        2.5
                     )
                );
                
                $myChart->series[] = array(
                    'name' => 'Berlin',
                    'data' => array(
                        - 0.9,
                        0.6,
                        3.5,
                        8.4,
                        13.5,
                        17.0,
                        18.6,
                        17.9,
                        14.3,
                        9.0,
                        3.9,
                        1.0
                    )
                );
                
                $myChart->series[] = array(
                    'name' => 'London',
                    'data' => array(
                        3.9,
                        4.2,
                        5.7,
                        8.5,
                        11.9,
                        15.2,
                        17.0,
                        16.6,
                        14.2,
                        10.3,
                        6.6,
                        4.8
                    )
                );

                $myChart->tooltip->formatter = $this->Highcharts->createJsExpr(
                "function() { return '<b>'+ this.series.name +'</b><br/>'+ this.x +': '+ this.y +'°C';}");
                
                $this->set(compact('myChart', 'chartName'));
        }

        public function pie() {
                
                $chartData = array(
                    array(
                        'name' => 'Chrome',
                        'y' => 45.0,
                        'sliced' => true,
                        'selected' => true
                    ),
                    array('IE', 26.8),
                    array('Firefox', 12.8),
                    array('Safari', 8.5),
                    array('Opera', 6.2),
                    array('Others', 0.7)
                );

                $chartName = 'Pie Chart';

                $pieChart = $this->Highcharts->create($chartName, 'pie');

                $this->Highcharts->setChartParams($chartName, array(
                    'renderTo' => 'piewrapper', // div to display chart inside
                    'chartWidth' => 800,
                    'chartHeight' => 600,
                    'chartMarginTop' => 60,
                    'chartMarginLeft' => 90,
                    'chartMarginRight' => 30,
                    'chartMarginBottom' => 110,
                    'chartSpacingRight' => 10,
                    'chartSpacingBottom' => 15,
                    'chartSpacingLeft' => 0,
                    'chartAlignTicks' => FALSE,
                    //'chartBackgroundColorLinearGradient' 	=> array(0,0,0,300),
                    //'chartBackgroundColorStops'	=> array(array(0,'rgb(217, 217, 217)'),array(1,'rgb(255, 255, 255)')),
                    'title' => 'Browser Usage Statistics',
                    'titleAlign' => 'left',
                    'titleFloating' => TRUE,
                    'titleStyleFont' => '18px Metrophobic, Arial, sans-serif',
                    'titleStyleColor' => '#0099ff',
                    'titleX' => 20,
                    'titleY' => 20,
                    'legendEnabled' => TRUE,
                    'legendLayout' => 'horizontal',
                    'legendAlign' => 'center',
                    'legendVerticalAlign ' => 'bottom',
                    'legendItemStyle' => array('color' => '#222'),
                    'legendBackgroundColorLinearGradient' => array(0, 0, 0, 25),
                    'legendBackgroundColorStops' => array(array(0, 'rgb(217, 217, 217)'), array(1, 'rgb(255, 255, 255)')),
                    'tooltipEnabled' => FALSE,
                        //'tooltipBackgroundColorLinearGradient' 	=> array(0,0,0,50),   // triggers js error
                        //'tooltipBackgroundColorStops' 		=> array(array(0,'rgb(217, 217, 217)'),array(1,'rgb(255, 255, 255)')),
                        )
                );

                $series = $this->Highcharts->addChartSeries();
                $series->addName('Browser Share')
                        ->addData($chartData);
                $pieChart->addSeries($series);
                
                $this->set(compact('chartName'));
        }

        public function scatter() {

                $chartName = 'Scatter Chart';
                
                $myChart = $this->Highcharts->createChart();
                $myChart->chart->renderTo = 'scatterwrapper';
                $myChart->chart->type = 'scatter';
                $myChart->chart->zoomType = "xy";
                $myChart->title->text = 'Height Versus Weight of 507 Individuals by Gender';
                $myChart->subtitle->text = 'Source: Heinz 2003';
                $myChart->xAxis->title->enabled = 1;
                $myChart->xAxis->title->text = 'Height (cm)';
                $myChart->xAxis->startOnTick = 1;
                $myChart->xAxis->endOnTick = 1;
                $myChart->xAxis->showLastLabel = 1;
                $myChart->yAxis->title->text = "Weight (kg)";
                $myChart->tooltip->formatter = $this->Highcharts->createJsExpr(
                    "function() {
                        return ''+
                        this.x +' cm, '+ this.y +' kg';
                    }"
                );
                $myChart->legend->layout = 'vertical';
                $myChart->legend->align = 'left';
                $myChart->legend->verticalAlign = 'top';
                $myChart->legend->x = 100;
                $myChart->legend->y = 70;
                $myChart->legend->floating = 1;
                $myChart->legend->backgroundColor = '#FFFFFF';
                $myChart->legend->borderWidth = 1;
                $myChart->plotOptions->scatter->marker->radius = 5;
                $myChart->plotOptions->scatter->marker->states->hover->enabled = 1;
                $myChart->plotOptions->scatter->marker->states->hover->lineColor = "rgb(100,100,100)";
                $myChart->plotOptions->scatter->states->hover->marker->enabled = false;
                $myChart->series[] = array(
                    'name' => "Female",
                    'color' => "rgba(223, 83, 83, .5)",
                    'data' => array(
                        array(
                            161.2,
                            51.6
                        ),
                        array(
                            167.5,
                            59.0
                        ),
                        array(
                            159.5,
                            49.2
                        ),
                        array(
                            157.0,
                            63.0
                        ),
                        array(
                            155.8,
                            53.6
                        ),
                        array(
                            170.0,
                            59.0
                        ),
                        array(
                            159.1,
                            47.6
                        ),
                        array(
                            166.0,
                            69.8
                        ),
                        array(
                            176.2,
                            66.8
                        ),
                        array(
                            160.2,
                            75.2
                        ),
                        array(
                            172.5,
                            55.2
                        ),
                        array(
                            170.9,
                            54.2
                        ),
                        array(
                            172.9,
                            62.5
                        ),
                        array(
                            153.4,
                            42.0
                        ),
                        array(
                            160.0,
                            50.0
                        ),
                        array(
                            147.2,
                            49.8
                        ),
                        array(
                            168.2,
                            49.2
                        ),
                        array(
                            175.0,
                            73.2
                        ),
                        array(
                            157.0,
                            47.8
                        ),
                        array(
                            167.6,
                            68.8
                        ),
                        array(
                            159.5,
                            50.6
                        ),
                        array(
                            175.0,
                            82.5
                        ),
                        array(
                            166.8,
                            57.2
                        ),
                        array(
                            176.5,
                            87.8
                        ),
                        array(
                            170.2,
                            72.8
                        ),
                        array(
                            174.0,
                            54.5
                        ),
                        array(
                            173.0,
                            59.8
                        ),
                        array(
                            179.9,
                            67.3
                        ),
                        array(
                            170.5,
                            67.8
                        ),
                        array(
                            160.0,
                            47.0
                        ),
                        array(
                            154.4,
                            46.2
                        ),
                        array(
                            162.0,
                            55.0
                        ),
                        array(
                            176.5,
                            83.0
                        ),
                        array(
                            160.0,
                            54.4
                        ),
                        array(
                            152.0,
                            45.8
                        ),
                        array(
                            162.1,
                            53.6
                        ),
                        array(
                            170.0,
                            73.2
                        ),
                        array(
                            160.2,
                            52.1
                        ),
                        array(
                            161.3,
                            67.9
                        ),
                        array(
                            166.4,
                            56.6
                        ),
                        array(
                            168.9,
                            62.3
                        ),
                        array(
                            163.8,
                            58.5
                        ),
                        array(
                            167.6,
                            54.5
                        ),
                        array(
                            160.0,
                            50.2
                        ),
                        array(
                            161.3,
                            60.3
                        ),
                        array(
                            167.6,
                            58.3
                        ),
                        array(
                            165.1,
                            56.2
                        ),
                        array(
                            160.0,
                            50.2
                        ),
                        array(
                            170.0,
                            72.9
                        ),
                        array(
                            157.5,
                            59.8
                        ),
                        array(
                            167.6,
                            61.0
                        ),
                        array(
                            160.7,
                            69.1
                        ),
                        array(
                            163.2,
                            55.9
                        ),
                        array(
                            152.4,
                            46.5
                        ),
                        array(
                            157.5,
                            54.3
                        ),
                        array(
                            168.3,
                            54.8
                        ),
                        array(
                            180.3,
                            60.7
                        ),
                        array(
                            165.5,
                            60.0
                        ),
                        array(
                            165.0,
                            62.0
                        ),
                        array(
                            164.5,
                            60.3
                        ),
                        array(
                            156.0,
                            52.7
                        ),
                        array(
                            160.0,
                            74.3
                        ),
                        array(
                            163.0,
                            62.0
                        ),
                        array(
                            165.7,
                            73.1
                        ),
                        array(
                            161.0,
                            80.0
                        ),
                        array(
                            162.0,
                            54.7
                        ),
                        array(
                            166.0,
                            53.2
                        ),
                        array(
                            174.0,
                            75.7
                        ),
                        array(
                            172.7,
                            61.1
                        ),
                        array(
                            167.6,
                            55.7
                        ),
                        array(
                            151.1,
                            48.7
                        ),
                        array(
                            164.5,
                            52.3
                        ),
                        array(
                            163.5,
                            50.0
                        ),
                        array(
                            152.0,
                            59.3
                        ),
                        array(
                            169.0,
                            62.5
                        ),
                        array(
                            164.0,
                            55.7
                        ),
                        array(
                            161.2,
                            54.8
                        ),
                        array(
                            155.0,
                            45.9
                        ),
                        array(
                            170.0,
                            70.6
                        ),
                        array(
                            176.2,
                            67.2
                        ),
                        array(
                            170.0,
                            69.4
                        ),
                        array(
                            162.5,
                            58.2
                        ),
                        array(
                            170.3,
                            64.8
                        ),
                        array(
                            164.1,
                            71.6
                        ),
                        array(
                            169.5,
                            52.8
                        ),
                        array(
                            163.2,
                            59.8
                        ),
                        array(
                            154.5,
                            49.0
                        ),
                        array(
                            159.8,
                            50.0
                        ),
                        array(
                            173.2,
                            69.2
                        ),
                        array(
                            170.0,
                            55.9
                        ),
                        array(
                            161.4,
                            63.4
                        ),
                        array(
                            169.0,
                            58.2
                        ),
                        array(
                            166.2,
                            58.6
                        ),
                        array(
                            159.4,
                            45.7
                        ),
                        array(
                            162.5,
                            52.2
                        ),
                        array(
                            159.0,
                            48.6
                        ),
                        array(
                            162.8,
                            57.8
                        ),
                        array(
                            159.0,
                            55.6
                        ),
                        array(
                            179.8,
                            66.8
                        ),
                        array(
                            162.9,
                            59.4
                        ),
                        array(
                            161.0,
                            53.6
                        ),
                        array(
                            151.1,
                            73.2
                        ),
                        array(
                            168.2,
                            53.4
                        ),
                        array(
                            168.9,
                            69.0
                        ),
                        array(
                            173.2,
                            58.4
                        ),
                        array(
                            171.8,
                            56.2
                        ),
                        array(
                            178.0,
                            70.6
                        ),
                        array(
                            164.3,
                            59.8
                        ),
                        array(
                            163.0,
                            72.0
                        ),
                        array(
                            168.5,
                            65.2
                        ),
                        array(
                            166.8,
                            56.6
                        ),
                        array(
                            172.7,
                            105.2
                        ),
                        array(
                            163.5,
                            51.8
                        ),
                        array(
                            169.4,
                            63.4
                        ),
                        array(
                            167.8,
                            59.0
                        ),
                        array(
                            159.5,
                            47.6
                        ),
                        array(
                            167.6,
                            63.0
                        ),
                        array(
                            161.2,
                            55.2
                        ),
                        array(
                            160.0,
                            45.0
                        ),
                        array(
                            163.2,
                            54.0
                        ),
                        array(
                            162.2,
                            50.2
                        ),
                        array(
                            161.3,
                            60.2
                        ),
                        array(
                            149.5,
                            44.8
                        ),
                        array(
                            157.5,
                            58.8
                        ),
                        array(
                            163.2,
                            56.4
                        ),
                        array(
                            172.7,
                            62.0
                        ),
                        array(
                            155.0,
                            49.2
                        ),
                        array(
                            156.5,
                            67.2
                        ),
                        array(
                            164.0,
                            53.8
                        ),
                        array(
                            160.9,
                            54.4
                        ),
                        array(
                            162.8,
                            58.0
                        ),
                        array(
                            167.0,
                            59.8
                        ),
                        array(
                            160.0,
                            54.8
                        ),
                        array(
                            160.0,
                            43.2
                        ),
                        array(
                            168.9,
                            60.5
                        ),
                        array(
                            158.2,
                            46.4
                        ),
                        array(
                            156.0,
                            64.4
                        ),
                        array(
                            160.0,
                            48.8
                        ),
                        array(
                            167.1,
                            62.2
                        ),
                        array(
                            158.0,
                            55.5
                        ),
                        array(
                            167.6,
                            57.8
                        ),
                        array(
                            156.0,
                            54.6
                        ),
                        array(
                            162.1,
                            59.2
                        ),
                        array(
                            173.4,
                            52.7
                        ),
                        array(
                            159.8,
                            53.2
                        ),
                        array(
                            170.5,
                            64.5
                        ),
                        array(
                            159.2,
                            51.8
                        ),
                        array(
                            157.5,
                            56.0
                        ),
                        array(
                            161.3,
                            63.6
                        ),
                        array(
                            162.6,
                            63.2
                        ),
                        array(
                            160.0,
                            59.5
                        ),
                        array(
                            168.9,
                            56.8
                        ),
                        array(
                            165.1,
                            64.1
                        ),
                        array(
                            162.6,
                            50.0
                        ),
                        array(
                            165.1,
                            72.3
                        ),
                        array(
                            166.4,
                            55.0
                        ),
                        array(
                            160.0,
                            55.9
                        ),
                        array(
                            152.4,
                            60.4
                        ),
                        array(
                            170.2,
                            69.1
                        ),
                        array(
                            162.6,
                            84.5
                        ),
                        array(
                            170.2,
                            55.9
                        ),
                        array(
                            158.8,
                            55.5
                        ),
                        array(
                            172.7,
                            69.5
                        ),
                        array(
                            167.6,
                            76.4
                        ),
                        array(
                            162.6,
                            61.4
                        ),
                        array(
                            167.6,
                            65.9
                        ),
                        array(
                            156.2,
                            58.6
                        ),
                        array(
                            175.2,
                            66.8
                        ),
                        array(
                            172.1,
                            56.6
                        ),
                        array(
                            162.6,
                            58.6
                        ),
                        array(
                            160.0,
                            55.9
                        ),
                        array(
                            165.1,
                            59.1
                        ),
                        array(
                            182.9,
                            81.8
                        ),
                        array(
                            166.4,
                            70.7
                        ),
                        array(
                            165.1,
                            56.8
                        ),
                        array(
                            177.8,
                            60.0
                        ),
                        array(
                            165.1,
                            58.2
                        ),
                        array(
                            175.3,
                            72.7
                        ),
                        array(
                            154.9,
                            54.1
                        ),
                        array(
                            158.8,
                            49.1
                        ),
                        array(
                            172.7,
                            75.9
                        ),
                        array(
                            168.9,
                            55.0
                        ),
                        array(
                            161.3,
                            57.3
                        ),
                        array(
                            167.6,
                            55.0
                        ),
                        array(
                            165.1,
                            65.5
                        ),
                        array(
                            175.3,
                            65.5
                        ),
                        array(
                            157.5,
                            48.6
                        ),
                        array(
                            163.8,
                            58.6
                        ),
                        array(
                            167.6,
                            63.6
                        ),
                        array(
                            165.1,
                            55.2
                        ),
                        array(
                            165.1,
                            62.7
                        ),
                        array(
                            168.9,
                            56.6
                        ),
                        array(
                            162.6,
                            53.9
                        ),
                        array(
                            164.5,
                            63.2
                        ),
                        array(
                            176.5,
                            73.6
                        ),
                        array(
                            168.9,
                            62.0
                        ),
                        array(
                            175.3,
                            63.6
                        ),
                        array(
                            159.4,
                            53.2
                        ),
                        array(
                            160.0,
                            53.4
                        ),
                        array(
                            170.2,
                            55.0
                        ),
                        array(
                            162.6,
                            70.5
                        ),
                        array(
                            167.6,
                            54.5
                        ),
                        array(
                            162.6,
                            54.5
                        ),
                        array(
                            160.7,
                            55.9
                        ),
                        array(
                            160.0,
                            59.0
                        ),
                        array(
                            157.5,
                            63.6
                        ),
                        array(
                            162.6,
                            54.5
                        ),
                        array(
                            152.4,
                            47.3
                        ),
                        array(
                            170.2,
                            67.7
                        ),
                        array(
                            165.1,
                            80.9
                        ),
                        array(
                            172.7,
                            70.5
                        ),
                        array(
                            165.1,
                            60.9
                        ),
                        array(
                            170.2,
                            63.6
                        ),
                        array(
                            170.2,
                            54.5
                        ),
                        array(
                            170.2,
                            59.1
                        ),
                        array(
                            161.3,
                            70.5
                        ),
                        array(
                            167.6,
                            52.7
                        ),
                        array(
                            167.6,
                            62.7
                        ),
                        array(
                            165.1,
                            86.3
                        ),
                        array(
                            162.6,
                            66.4
                        ),
                        array(
                            152.4,
                            67.3
                        ),
                        array(
                            168.9,
                            63.0
                        ),
                        array(
                            170.2,
                            73.6
                        ),
                        array(
                            175.2,
                            62.3
                        ),
                        array(
                            175.2,
                            57.7
                        ),
                        array(
                            160.0,
                            55.4
                        ),
                        array(
                            165.1,
                            104.1
                        ),
                        array(
                            174.0,
                            55.5
                        ),
                        array(
                            170.2,
                            77.3
                        ),
                        array(
                            160.0,
                            80.5
                        ),
                        array(
                            167.6,
                            64.5
                        ),
                        array(
                            167.6,
                            72.3
                        ),
                        array(
                            167.6,
                            61.4
                        ),
                        array(
                            154.9,
                            58.2
                        ),
                        array(
                            162.6,
                            81.8
                        ),
                        array(
                            175.3,
                            63.6
                        ),
                        array(
                            171.4,
                            53.4
                        ),
                        array(
                            157.5,
                            54.5
                        ),
                        array(
                            165.1,
                            53.6
                        ),
                        array(
                            160.0,
                            60.0
                        ),
                        array(
                            174.0,
                            73.6
                        ),
                        array(
                            162.6,
                            61.4
                        ),
                        array(
                            174.0,
                            55.5
                        ),
                        array(
                            162.6,
                            63.6
                        ),
                        array(
                            161.3,
                            60.9
                        ),
                        array(
                            156.2,
                            60.0
                        ),
                        array(
                            149.9,
                            46.8
                        ),
                        array(
                            169.5,
                            57.3
                        ),
                        array(
                            160.0,
                            64.1
                        ),
                        array(
                            175.3,
                            63.6
                        ),
                        array(
                            169.5,
                            67.3
                        ),
                        array(
                            160.0,
                            75.5
                        ),
                        array(
                            172.7,
                            68.2
                        ),
                        array(
                            162.6,
                            61.4
                        ),
                        array(
                            157.5,
                            76.8
                        ),
                        array(
                            176.5,
                            71.8
                        ),
                        array(
                            164.4,
                            55.5
                        ),
                        array(
                            160.7,
                            48.6
                        ),
                        array(
                            174.0,
                            66.4
                        ),
                        array(
                            163.8,
                            67.3
                        )
                    )
                );

                $myChart->series[] = array(
                    'name' => "Male",
                    'color' => "rgba(119, 152, 191, .5)",
                    'data' => array(
                        array(
                            174.0,
                            65.6
                        ),
                        array(
                            175.3,
                            71.8
                        ),
                        array(
                            193.5,
                            80.7
                        ),
                        array(
                            186.5,
                            72.6
                        ),
                        array(
                            187.2,
                            78.8
                        ),
                        array(
                            181.5,
                            74.8
                        ),
                        array(
                            184.0,
                            86.4
                        ),
                        array(
                            184.5,
                            78.4
                        ),
                        array(
                            175.0,
                            62.0
                        ),
                        array(
                            184.0,
                            81.6
                        ),
                        array(
                            180.0,
                            76.6
                        ),
                        array(
                            177.8,
                            83.6
                        ),
                        array(
                            192.0,
                            90.0
                        ),
                        array(
                            176.0,
                            74.6
                        ),
                        array(
                            174.0,
                            71.0
                        ),
                        array(
                            184.0,
                            79.6
                        ),
                        array(
                            192.7,
                            93.8
                        ),
                        array(
                            171.5,
                            70.0
                        ),
                        array(
                            173.0,
                            72.4
                        ),
                        array(
                            176.0,
                            85.9
                        ),
                        array(
                            176.0,
                            78.8
                        ),
                        array(
                            180.5,
                            77.8
                        ),
                        array(
                            172.7,
                            66.2
                        ),
                        array(
                            176.0,
                            86.4
                        ),
                        array(
                            173.5,
                            81.8
                        ),
                        array(
                            178.0,
                            89.6
                        ),
                        array(
                            180.3,
                            82.8
                        ),
                        array(
                            180.3,
                            76.4
                        ),
                        array(
                            164.5,
                            63.2
                        ),
                        array(
                            173.0,
                            60.9
                        ),
                        array(
                            183.5,
                            74.8
                        ),
                        array(
                            175.5,
                            70.0
                        ),
                        array(
                            188.0,
                            72.4
                        ),
                        array(
                            189.2,
                            84.1
                        ),
                        array(
                            172.8,
                            69.1
                        ),
                        array(
                            170.0,
                            59.5
                        ),
                        array(
                            182.0,
                            67.2
                        ),
                        array(
                            170.0,
                            61.3
                        ),
                        array(
                            177.8,
                            68.6
                        ),
                        array(
                            184.2,
                            80.1
                        ),
                        array(
                            186.7,
                            87.8
                        ),
                        array(
                            171.4,
                            84.7
                        ),
                        array(
                            172.7,
                            73.4
                        ),
                        array(
                            175.3,
                            72.1
                        ),
                        array(
                            180.3,
                            82.6
                        ),
                        array(
                            182.9,
                            88.7
                        ),
                        array(
                            188.0,
                            84.1
                        ),
                        array(
                            177.2,
                            94.1
                        ),
                        array(
                            172.1,
                            74.9
                        ),
                        array(
                            167.0,
                            59.1
                        ),
                        array(
                            169.5,
                            75.6
                        ),
                        array(
                            174.0,
                            86.2
                        ),
                        array(
                            172.7,
                            75.3
                        ),
                        array(
                            182.2,
                            87.1
                        ),
                        array(
                            164.1,
                            55.2
                        ),
                        array(
                            163.0,
                            57.0
                        ),
                        array(
                            171.5,
                            61.4
                        ),
                        array(
                            184.2,
                            76.8
                        ),
                        array(
                            174.0,
                            86.8
                        ),
                        array(
                            174.0,
                            72.2
                        ),
                        array(
                            177.0,
                            71.6
                        ),
                        array(
                            186.0,
                            84.8
                        ),
                        array(
                            167.0,
                            68.2
                        ),
                        array(
                            171.8,
                            66.1
                        ),
                        array(
                            182.0,
                            72.0
                        ),
                        array(
                            167.0,
                            64.6
                        ),
                        array(
                            177.8,
                            74.8
                        ),
                        array(
                            164.5,
                            70.0
                        ),
                        array(
                            192.0,
                            101.6
                        ),
                        array(
                            175.5,
                            63.2
                        ),
                        array(
                            171.2,
                            79.1
                        ),
                        array(
                            181.6,
                            78.9
                        ),
                        array(
                            167.4,
                            67.7
                        ),
                        array(
                            181.1,
                            66.0
                        ),
                        array(
                            177.0,
                            68.2
                        ),
                        array(
                            174.5,
                            63.9
                        ),
                        array(
                            177.5,
                            72.0
                        ),
                        array(
                            170.5,
                            56.8
                        ),
                        array(
                            182.4,
                            74.5
                        ),
                        array(
                            197.1,
                            90.9
                        ),
                        array(
                            180.1,
                            93.0
                        ),
                        array(
                            175.5,
                            80.9
                        ),
                        array(
                            180.6,
                            72.7
                        ),
                        array(
                            184.4,
                            68.0
                        ),
                        array(
                            175.5,
                            70.9
                        ),
                        array(
                            180.6,
                            72.5
                        ),
                        array(
                            177.0,
                            72.5
                        ),
                        array(
                            177.1,
                            83.4
                        ),
                        array(
                            181.6,
                            75.5
                        ),
                        array(
                            176.5,
                            73.0
                        ),
                        array(
                            175.0,
                            70.2
                        ),
                        array(
                            174.0,
                            73.4
                        ),
                        array(
                            165.1,
                            70.5
                        ),
                        array(
                            177.0,
                            68.9
                        ),
                        array(
                            192.0,
                            102.3
                        ),
                        array(
                            176.5,
                            68.4
                        ),
                        array(
                            169.4,
                            65.9
                        ),
                        array(
                            182.1,
                            75.7
                        ),
                        array(
                            179.8,
                            84.5
                        ),
                        array(
                            175.3,
                            87.7
                        ),
                        array(
                            184.9,
                            86.4
                        ),
                        array(
                            177.3,
                            73.2
                        ),
                        array(
                            167.4,
                            53.9
                        ),
                        array(
                            178.1,
                            72.0
                        ),
                        array(
                            168.9,
                            55.5
                        ),
                        array(
                            157.2,
                            58.4
                        ),
                        array(
                            180.3,
                            83.2
                        ),
                        array(
                            170.2,
                            72.7
                        ),
                        array(
                            177.8,
                            64.1
                        ),
                        array(
                            172.7,
                            72.3
                        ),
                        array(
                            165.1,
                            65.0
                        ),
                        array(
                            186.7,
                            86.4
                        ),
                        array(
                            165.1,
                            65.0
                        ),
                        array(
                            174.0,
                            88.6
                        ),
                        array(
                            175.3,
                            84.1
                        ),
                        array(
                            185.4,
                            66.8
                        ),
                        array(
                            177.8,
                            75.5
                        ),
                        array(
                            180.3,
                            93.2
                        ),
                        array(
                            180.3,
                            82.7
                        ),
                        array(
                            177.8,
                            58.0
                        ),
                        array(
                            177.8,
                            79.5
                        ),
                        array(
                            177.8,
                            78.6
                        ),
                        array(
                            177.8,
                            71.8
                        ),
                        array(
                            177.8,
                            116.4
                        ),
                        array(
                            163.8,
                            72.2
                        ),
                        array(
                            188.0,
                            83.6
                        ),
                        array(
                            198.1,
                            85.5
                        ),
                        array(
                            175.3,
                            90.9
                        ),
                        array(
                            166.4,
                            85.9
                        ),
                        array(
                            190.5,
                            89.1
                        ),
                        array(
                            166.4,
                            75.0
                        ),
                        array(
                            177.8,
                            77.7
                        ),
                        array(
                            179.7,
                            86.4
                        ),
                        array(
                            172.7,
                            90.9
                        ),
                        array(
                            190.5,
                            73.6
                        ),
                        array(
                            185.4,
                            76.4
                        ),
                        array(
                            168.9,
                            69.1
                        ),
                        array(
                            167.6,
                            84.5
                        ),
                        array(
                            175.3,
                            64.5
                        ),
                        array(
                            170.2,
                            69.1
                        ),
                        array(
                            190.5,
                            108.6
                        ),
                        array(
                            177.8,
                            86.4
                        ),
                        array(
                            190.5,
                            80.9
                        ),
                        array(
                            177.8,
                            87.7
                        ),
                        array(
                            184.2,
                            94.5
                        ),
                        array(
                            176.5,
                            80.2
                        ),
                        array(
                            177.8,
                            72.0
                        ),
                        array(
                            180.3,
                            71.4
                        ),
                        array(
                            171.4,
                            72.7
                        ),
                        array(
                            172.7,
                            84.1
                        ),
                        array(
                            172.7,
                            76.8
                        ),
                        array(
                            177.8,
                            63.6
                        ),
                        array(
                            177.8,
                            80.9
                        ),
                        array(
                            182.9,
                            80.9
                        ),
                        array(
                            170.2,
                            85.5
                        ),
                        array(
                            167.6,
                            68.6
                        ),
                        array(
                            175.3,
                            67.7
                        ),
                        array(
                            165.1,
                            66.4
                        ),
                        array(
                            185.4,
                            102.3
                        ),
                        array(
                            181.6,
                            70.5
                        ),
                        array(
                            172.7,
                            95.9
                        ),
                        array(
                            190.5,
                            84.1
                        ),
                        array(
                            179.1,
                            87.3
                        ),
                        array(
                            175.3,
                            71.8
                        ),
                        array(
                            170.2,
                            65.9
                        ),
                        array(
                            193.0,
                            95.9
                        ),
                        array(
                            171.4,
                            91.4
                        ),
                        array(
                            177.8,
                            81.8
                        ),
                        array(
                            177.8,
                            96.8
                        ),
                        array(
                            167.6,
                            69.1
                        ),
                        array(
                            167.6,
                            82.7
                        ),
                        array(
                            180.3,
                            75.5
                        ),
                        array(
                            182.9,
                            79.5
                        ),
                        array(
                            176.5,
                            73.6
                        ),
                        array(
                            186.7,
                            91.8
                        ),
                        array(
                            188.0,
                            84.1
                        ),
                        array(
                            188.0,
                            85.9
                        ),
                        array(
                            177.8,
                            81.8
                        ),
                        array(
                            174.0,
                            82.5
                        ),
                        array(
                            177.8,
                            80.5
                        ),
                        array(
                            171.4,
                            70.0
                        ),
                        array(
                            185.4,
                            81.8
                        ),
                        array(
                            185.4,
                            84.1
                        ),
                        array(
                            188.0,
                            90.5
                        ),
                        array(
                            188.0,
                            91.4
                        ),
                        array(
                            182.9,
                            89.1
                        ),
                        array(
                            176.5,
                            85.0
                        ),
                        array(
                            175.3,
                            69.1
                        ),
                        array(
                            175.3,
                            73.6
                        ),
                        array(
                            188.0,
                            80.5
                        ),
                        array(
                            188.0,
                            82.7
                        ),
                        array(
                            175.3,
                            86.4
                        ),
                        array(
                            170.5,
                            67.7
                        ),
                        array(
                            179.1,
                            92.7
                        ),
                        array(
                            177.8,
                            93.6
                        ),
                        array(
                            175.3,
                            70.9
                        ),
                        array(
                            182.9,
                            75.0
                        ),
                        array(
                            170.8,
                            93.2
                        ),
                        array(
                            188.0,
                            93.2
                        ),
                        array(
                            180.3,
                            77.7
                        ),
                        array(
                            177.8,
                            61.4
                        ),
                        array(
                            185.4,
                            94.1
                        ),
                        array(
                            168.9,
                            75.0
                        ),
                        array(
                            185.4,
                            83.6
                        ),
                        array(
                            180.3,
                            85.5
                        ),
                        array(
                            174.0,
                            73.9
                        ),
                        array(
                            167.6,
                            66.8
                        ),
                        array(
                            182.9,
                            87.3
                        ),
                        array(
                            160.0,
                            72.3
                        ),
                        array(
                            180.3,
                            88.6
                        ),
                        array(
                            167.6,
                            75.5
                        ),
                        array(
                            186.7,
                            101.4
                        ),
                        array(
                            175.3,
                            91.1
                        ),
                        array(
                            175.3,
                            67.3
                        ),
                        array(
                            175.9,
                            77.7
                        ),
                        array(
                            175.3,
                            81.8
                        ),
                        array(
                            179.1,
                            75.5
                        ),
                        array(
                            181.6,
                            84.5
                        ),
                        array(
                            177.8,
                            76.6
                        ),
                        array(
                            182.9,
                            85.0
                        ),
                        array(
                            177.8,
                            102.5
                        ),
                        array(
                            184.2,
                            77.3
                        ),
                        array(
                            179.1,
                            71.8
                        ),
                        array(
                            176.5,
                            87.9
                        ),
                        array(
                            188.0,
                            94.3
                        ),
                        array(
                            174.0,
                            70.9
                        ),
                        array(
                            167.6,
                            64.5
                        ),
                        array(
                            170.2,
                            77.3
                        ),
                        array(
                            167.6,
                            72.3
                        ),
                        array(
                            188.0,
                            87.3
                        ),
                        array(
                            174.0,
                            80.0
                        ),
                        array(
                            176.5,
                            82.3
                        ),
                        array(
                            180.3,
                            73.6
                        ),
                        array(
                            167.6,
                            74.1
                        ),
                        array(
                            188.0,
                            85.9
                        ),
                        array(
                            180.3,
                            73.2
                        ),
                        array(
                            167.6,
                            76.3
                        ),
                        array(
                            183.0,
                            65.9
                        ),
                        array(
                            183.0,
                            90.9
                        ),
                        array(
                            179.1,
                            89.1
                        ),
                        array(
                            170.2,
                            62.3
                        ),
                        array(
                            177.8,
                            82.7
                        ),
                        array(
                            179.1,
                            79.1
                        ),
                        array(
                            190.5,
                            98.2
                        ),
                        array(
                            177.8,
                            84.1
                        ),
                        array(
                            180.3,
                            83.2
                        ),
                        array(
                            180.3,
                            83.2
                        )
                    )
                );

                $this->set(compact('myChart', 'chartName'));
        }

         public function spline() {

                $chartName = 'Spline Chart';

                $myChart = $this->Highcharts->createChart();


                $myChart->chart->renderTo = 'splinewrapper';
                $myChart->chart->type = 'spline';
                $myChart->title->text = 'Average fruit consumption during one week';
                $myChart->legend->layout = 'vertical';
                $myChart->legend->align = 'left';
                $myChart->legend->verticalAlign = 'top';
                $myChart->legend->x = 150;
                $myChart->legend->y = 100;
                $myChart->legend->floating = 1;
                $myChart->legend->borderWidth = 1;
                $myChart->legend->backgroundColor = '#FFFFFF';
                $myChart->xAxis->categories = array(
                    'Monday',
                    'Tuesday',
                    'Wednesday',
                    'Thursday',
                    'Friday',
                    'Saturday',
                    'Sunday'
                );
                $myChart->yAxis->title->text = 'Fruit units';
                $myChart->tooltip->formatter = $this->Highcharts->createJsExpr("function() { return ''+ this.x +': '+ this.y +' units'; }");
                $myChart->credits->enabled = true;
                $myChart->plotOptions->areaspline->fillOpacity = 0.5;
                $myChart->series[] = array(
                    'name' => 'John',
                    'data' => array(
                        3,
                        4,
                        3,
                        5,
                        4,
                        10,
                        12
                    )
                );
                $myChart->series[] = array(
                    'name' => 'Jane',
                    'data' => array(
                        1,
                        3,
                        4,
                        3,
                        3,
                        5,
                        4
                    )
                );
                
                $this->set(compact('myChart', 'chartName'));
        }
        
        public function bubble() {
                $chartName = 'Bubble Chart';
                $myChart = $this->Highcharts->createChart();
                $myChart->chart->renderTo = 'container';
                $myChart->chart->type = 'bubble';
                $myChart->chart->zoomType = 'xy';
                $myChart->title->text = 'Highcharts Bubbles';

                $myChart->series = array(
                    array(
                        'data' => array(
                            array(97, 36, 79),
                            array(94, 74, 60),
                            array(68, 76, 58),
                            array(64, 87, 56),
                            array(68, 27, 73),
                            array(74, 99, 42),
                            array(7, 93, 87),
                            array(51, 69, 40),
                            array(38, 23, 33),
                            array(57, 86, 31)
                        )
                    ),
                    array(
                        'data' => array(
                            array(25, 10, 87),
                            array(2, 75, 59),
                            array(11, 54, 8),
                            array(86, 55, 93),
                            array(5, 3, 58),
                            array(90, 63, 44),
                            array(91, 33, 17),
                            array(97, 3, 56),
                            array(15, 67, 48),
                            array(54, 25, 81)
                        )
                    ),
                    array(
                        'data' => array(
                            array(47, 47, 21),
                            array(20, 12, 4),
                            array(6, 76, 91),
                            array(38, 30, 60),
                            array(57, 98, 64),
                            array(61, 17, 80),
                            array(83, 60, 13),
                            array(67, 78, 75),
                            array(64, 12, 10),
                            array(30, 77, 82)
                        )
                    )
                );
                
                $this->set(compact('myChart', 'chartName'));
        }
        
        public function bubble3d() {
                $chartName = '3D Bubble Chart';
                $myChart = $this->Highcharts->createChart();
                $myChart->chart->renderTo = 'bubblewrapper';
                $myChart->chart->type = 'bubble';
                $myChart->chart->plotBorderWidth = 1;
                $myChart->chart->zoomType = 'xy';
                $myChart->title->text = 'Highcharts bubbles with radial gradient fill';
                $myChart->xAxis->gridLineWidth = 1;
                $myChart->yAxis->startOnTick = false;
                $myChart->yAxis->endOnTick = false;

                $myChart->series = array(
                    array(
                        'data' => array(
                            array(9, 81, 63),
                            array(98, 5, 89),
                            array(51, 50, 73),
                            array(41, 22, 14),
                            array(58, 24, 20),
                            array(78, 37, 34),
                            array(55, 56, 53),
                            array(18, 45, 70),
                            array(42, 44, 28),
                            array(3, 52, 59),
                            array(31, 18, 97),
                            array(79, 91, 63),
                            array(93, 23, 23),
                            array(44, 83, 22)
                        ),
                        'marker' => array(
                            'fillColor' => array(
                                'radialGradient' => array(
                                    'cx' => 0.4,
                                    'cy' => 0.3,
                                    'r' => 0.7
                                ),
                                'stops' => array(
                                    array(0, 'rgba(255,255,255,0.5)'),
                                    array(1, 'rgba(69,114,167,0.5)')
                                )
                            )
                        )
                    ),
                    array(
                        'data' => array(
                            array(42, 38, 20),
                            array(6, 18, 1),
                            array(1, 93, 55),
                            array(57, 2, 90),
                            array(80, 76, 22),
                            array(11, 74, 96),
                            array(88, 56, 10),
                            array(30, 47, 49),
                            array(57, 62, 98),
                            array(4, 16, 16),
                            array(46, 10, 11),
                            array(22, 87, 89),
                            array(57, 91, 82),
                            array(45, 15, 98)
                        ),
                        'marker' => array(
                            'fillColor' => array(
                                'radialGradient' => array(
                                    'cx' => 0.4,
                                    'cy' => 0.3,
                                    'r' => 0.7
                                ),
                                'stops' => array(
                                    array(0, 'rgba(255,255,255,0.5)'),
                                    array(1, 'rgba(170,70,67,0.5)')
                                )
                            )
                        )
                    )
                );
                
                $this->set(compact('myChart', 'chartName'));
        }

}
