<?php
/**
 *  CakePHP Highcharts Plugin
 *
 *  Copyright (C) 2015 Kurn La Montagne / destinydriven
 *  <https://github.com/destinydriven>
 *
 *  Multi-licensed under:
 *      MPL <http://www.mozilla.org/MPL/MPL-1.1.html>
 *      LGPL <http://www.gnu.org/licenses/lgpl.html>
 *      GPL <http://www.gnu.org/licenses/gpl.html>
 *      Apache License, Version 2.0 <http://www.apache.org/licenses/LICENSE-2.0.html>
 */
namespace Highcharts\Controller\Component;

use Cake\Event\Event;
use Cake\Controller\Component;
use Cake\Controller\ComponentRegistry;
use Ghunti\HighchartsPHP\Highchart;
use Ghunti\HighchartsPHP\HighchartJsExpr;

class HighchartsComponent extends Component {

        public $controller;
        public $components = array('Session');
        public $registry;
        public $settings = array();
        public $chart;
        public $series;
        public $session;

/**
 * Constructor
 *
 * @param ComponentRegistry $registry A ComponentRegistry this component can use to lazy load its components
 * @param array $settings Array of configuration settings
 */
        public function __construct(ComponentRegistry $registry, $settings = array()) {
                parent::__construct($registry, $settings);
                $this->registry = $registry->getController();
                $this->settings = $settings;
                $this->session = $this->request->session();
        }

        
        public function initialize(array $config) {
                parent::initialize($config);
                if (!isset($this->controller->helpers['Highcharts.Highcharts'])) {
                        $this->controller->helpers[] = 'Highcharts.Highcharts';
                }
        }
        
        public function beforeRender(Event $event) {
                $this->session->write('Highcharts.Charts', $this->chart);
        }
        
/**
 * Creates a highcharts chart object.
 * @param int $chartType The chart type (Either 0 for HIGHCHART or 1 for HIGHSTOCK)
 * @param int $jsEngine  The javascript library to use (10 for ENGINE_JQUERY, 11 for ENGINE_MOOTOOLS or 12 for ENGINE_PROTOTYPE)
 * @return Highchart Highchart chart object of specified type .
 */
        public function createChart($chartType = 0, $jsEngine = 10) {               
                return $this->chart = new Highchart($chartType, $jsEngine);
        }
        
        public function createJsExpr($expression) {                               
                return $jsExpression = new HighchartJsExpr($expression);
        }
        

  /** add series to your chart
   * @param $seriesData - array, data provided in 1 of 3 HighCharts supported array formats (array, assoc array or mult-dimensional array)
   * @return void
   */
        public function addSeries($seriesData){
                if(!is_object($seriesData)){
                  die("Highcharts::addSeries() - series input format must be an object.");
                }

                if(is_object($this->chart->series)){   // if series is an object
                  return $this->chart->series = array($seriesData);
                } else if(is_array($this->chart->series)) {                        
                  return array_push($this->chart->series, $seriesData);
                }
        }        


/**
 * Add chart data.
 * Wrapper for HighRoller addData()
 * @param array $data An array of values.
 */
        public function addChartData($data) {
                if (isset($data)) {
                        $this->chart->addData($data);
                }
        }

}
