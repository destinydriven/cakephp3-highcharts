<?php
/**
 *  CakePHP Highcharts Plugin
 *
 * @property HighchartsComponent $Highcharts
 *
 *    Copyright (C) 2015 Kurn La Montagne / destinydriven
 *    <https://github.com/destinydriven>
 *
 *    Multi-licensed under:
 *        MPL <http://www.mozilla.org/MPL/MPL-1.1.html>
 *        LGPL <http://www.gnu.org/licenses/lgpl.html>
 *        GPL <http://www.gnu.org/licenses/gpl.html>
 *        Apache License, Version 2.0 <http://www.apache.org/licenses/LICENSE-2.0.html>
 */
namespace Highcharts\Controller;

use Highcharts\Controller\AppController;
use Highcharts\Controller\Component\HighchartsComponent;

class SingleSeriesDemoController extends AppController {

        public $name = 'SingleSeriesDemo';
        public $helpers = array('Html');
        public $uses = array();
        public $layout = 'Highcharts.demo';
        public $chartData = array(7.0, 6.9, 9.5, 14.5, 18.2, 21.5, 25.2, 26.5, 23.3, 18.3, 13.9, 9.6);
        public $chartData2 = array(3.0, 3.9, 5.5, 6.5, 9.2, 17.5, 8.2, 6.5, 13.3, 28.3, 23.9, 29.6);
        
        
        public function initialize() {
                parent::initialize();
                $this->loadComponent('Highcharts.Highcharts');
        }

        public function area() {

                $chartName = 'Area Chart';

                $myChart = $this->Highcharts->createChart();
                
                $myChart->title = array(
                    'text' => 'Monthly Average Temperature', 
                    'x' => 20,
                    'y' => 20,
                    'align' => 'left',
                    'styleFont' => '18px Metrophobic, Arial, sans-serif',
                    'styleColor' => '#0099ff',
                );
                
                $myChart->chart->renderTo = 'areawrapper';
                $myChart->chart->type = 'area';
                $myChart->chart->width =  800;
                $myChart->chart->height = 600;
                $myChart->chart->marginTop = 60;
                $myChart->chart->marginLeft = 90;
                $myChart->chart->marginRight = 30;
                $myChart->chart->marginBottom = 110;
                $myChart->chart->spacingRight = 10;
                $myChart->chart->spacingBottom = 15;
                $myChart->chart->spacingLeft = 0;
                $myChart->chart->alignTicks = FALSE;
                $myChart->chart->backgroundColor->linearGradient = array(0, 0, 0, 300);
                $myChart->chart->backgroundColor->stops = array(array(0, 'rgb(217, 217, 217)'), array(1, 'rgb(255, 255, 255)'));                
                
                $myChart->title->text = 'US and USSR nuclear stockpiles';
                $myChart->subtitle->text = "Source: <a href=\"http://thebulletin.metapress.com/content/c4120650912x74k7/fulltext.pdf\">thebulletin.metapress.com</a>";
                $myChart->xAxis->labels->formatter = $this->Highcharts->createJsExpr("function() { return this.value;}");
                $myChart->yAxis->title->text = 'Nuclear weapon states';
                $myChart->yAxis->labels->formatter = $this->Highcharts->createJsExpr("function() { return this.value / 1000 +'k';}");
                $myChart->tooltip->formatter = $this->Highcharts->createJsExpr("function() {
                return this.series.name +' produced <b>'+
                Highcharts.numberFormat(this.y, 0) +'</b><br/>warheads in '+ this.x;}");
                $myChart->plotOptions->area->marker->enabled = false;
                $myChart->plotOptions->area->marker->symbol = 'circle';
                $myChart->plotOptions->area->marker->radius = 2;
                $myChart->plotOptions->area->marker->states->hover->enabled = true;
                
                $myChart->legend->enabled = true;
                $myChart->legend->layout = 'horizontal';
                $myChart->legend->align = 'center';
                $myChart->legend->verticalAlign  = 'bottom';
                $myChart->legend->itemStyle = array('color' => '#222');
                $myChart->legend->backgroundColor->linearGradient = array(0, 0, 0, 25);
                $myChart->legend->backgroundColor->stops = array(array(0, 'rgb(217, 217, 217)'), array(1, 'rgb(255, 255, 255)'));

                $myChart->xAxis->labels->enabled = true;
                $myChart->xAxis->labels->align = 'right';
                $myChart->xAxis->labels->step = 2;
                $myChart->xAxis->labels->x = 5;
                $myChart->xAxis->labels->y = 20;
                $myChart->xAxis->categories = array('Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec');
                $myChart->yAxis->title->text = 'Units Sold';
                $myChart->enable->autoStep = false;
                // credits setting  [Highcharts.com  displayed on chart]
                $myChart->credits->enabled = true;
                $myChart->credits->text = 'Example.com';
                $myChart->credits->href = 'http://example.com';
                
                $myChart->series[] = array(
                    'name' => 'USA',
                    'data' => $this->chartData
                );

                $myChart->series[] = array(
                    'name' => 'USSR/Russia',
                    'data' => $this->chartData2
                );

                $this->set(compact('myChart', 'chartName'));
        }

        public function areaspline() {

                $chartName = 'AreaSpline Chart';

                $myChart = $this->Highcharts->createChart();

                $myChart->chart->renderTo = 'areasplinewrapper';
                $myChart->chart->type = 'areaspline';
                $myChart->title->text = 'Average fruit consumption during one week';
                $myChart->legend->layout = 'vertical';
                $myChart->legend->align = 'left';
                $myChart->legend->verticalAlign = 'top';
                $myChart->legend->x = 150;
                $myChart->legend->y = 100;
                $myChart->legend->floating = 1;
                $myChart->legend->borderWidth = 1;
                $myChart->legend->backgroundColor = '#FFFFFF';
                $myChart->xAxis->categories = array(
                    'Monday',
                    'Tuesday',
                    'Wednesday',
                    'Thursday',
                    'Friday',
                    'Saturday',
                    'Sunday'
                );
                $myChart->xAxis->plotBands = array(
                    'from' => 4.5,
                    'to' => 6.5,
                    'color' => 'rgba(68, 170, 213, .2)'
                );
                $myChart->yAxis->title->text = 'Fruit units';
                $myChart->tooltip->formatter = $this->Highcharts->createJsExpr("function() { return ''+ this.x +': '+ this.y +' units'; }");
                $myChart->credits->enabled = true;
                $myChart->plotOptions->areaspline->fillOpacity = 0.5;
                $myChart->series[] = array(
                    'name' => 'John',
                    'data' => array(
                        3,
                        4,
                        3,
                        5,
                        4,
                        10,
                        12
                    )
                );
                $myChart->series[] = array(
                    'name' => 'Jane',
                    'data' => array(
                        1,
                        3,
                        4,
                        3,
                        3,
                        5,
                        4
                    )
                );
                
                $this->set(compact('myChart', 'chartName'));
        }

        public function bar() {

                $chartName = 'Bar Chart';

                $myChart = $this->Highcharts->createChart();

                $myChart->chart->renderTo = 'barwrapper';
                $myChart->chart->type = 'bar';
                $myChart->title->text = 'Historic World Population by Region';
                $myChart->subtitle->text = 'Source: Wikipedia.org';
                $myChart->xAxis->categories = array(
                    'Africa',
                    'America',
                    'Asia',
                    'Europe',
                    'Oceania'
                );
                $myChart->xAxis->title->text = null;
                $myChart->yAxis->min = 0;
                $myChart->yAxis->title->text = 'Population (millions)';
                $myChart->yAxis->title->align = 'high';

                $myChart->tooltip->formatter = $this->Highcharts->createJsExpr( "function() {
                    return '' + this.series.name +': '+ this.y +' millions';}");

                $myChart->plotOptions->bar->dataLabels->enabled = 1;
                $myChart->legend->layout = 'vertical';
                $myChart->legend->align = 'right';
                $myChart->legend->verticalAlign = 'top';
                $myChart->legend->x = - 100;
                $myChart->legend->y = 100;
                $myChart->legend->floating = 1;
                $myChart->legend->borderWidth = 1;
                $myChart->legend->backgroundColor = '#FFFFFF';
                $myChart->legend->shadow = 1;
                $myChart->credits->enabled = false;

                $myChart->series[] = array(
                    'name' => 'Year 1800',
                    'data' => array(
                        107,
                        31,
                        635,
                        203,
                        2
                    )
                );

                $myChart->series[] = array(
                    'name' => 'Year 1900',
                    'data' => array(
                        133,
                        156,
                        947,
                        408,
                        6
                    )
                );

                $myChart->series[] = array(
                    'name' => 'Year 2008',
                    'data' => array(
                        973,
                        914,
                        4054,
                        732,
                        34
                    )
                );

                $this->set(compact('myChart', 'chartName'));
        }

        public function column() {

                $chartName = 'Column Chart';

                $myChart = $this->Highcharts->createChart();
                
                $myChart->chart->renderTo = 'columnwrapper';
                $myChart->chart->type = 'column';
                $myChart->title->text = 'Monthly Average Rainfall';
                $myChart->subtitle->text = 'Source: WorldClimate.com';

                $myChart->xAxis->categories = array(
                    'Jan',
                    'Feb',
                    'Mar',
                    'Apr',
                    'May',
                    'Jun',
                    'Jul',
                    'Aug',
                    'Sep',
                    'Oct',
                    'Nov',
                    'Dec'
                );

                $myChart->yAxis->min = 0;
                $myChart->yAxis->title->text = 'Rainfall (mm)';
                $myChart->legend->layout = 'vertical';
                $myChart->legend->backgroundColor = '#FFFFFF';
                $myChart->legend->align = 'left';
                $myChart->legend->verticalAlign = 'top';
                $myChart->legend->x = 100;
                $myChart->legend->y = 70;
                $myChart->legend->floating = 1;
                $myChart->legend->shadow = 1;

                $myChart->tooltip->formatter = $this->Highcharts->createJsExpr("function() {
                    return '' + this.x +': '+ this.y +' mm';}");

                $myChart->plotOptions->column->pointPadding = 0.2;
                $myChart->plotOptions->column->borderWidth = 0;

                $myChart->series[] = array(
                    'name' => 'Tokyo',
                    'data' => array(
                        49.9,
                        71.5,
                        106.4,
                        129.2,
                        144.0,
                        176.0,
                        135.6,
                        148.5,
                        216.4,
                        194.1,
                        95.6,
                        54.4
                    )
                );

                $myChart->series[] = array(
                    'name' => 'New York',
                    'data' => array(
                        83.6,
                        78.8,
                        98.5,
                        93.4,
                        106.0,
                        84.5,
                        105.0,
                        104.3,
                        91.2,
                        83.5,
                        106.6,
                        92.3
                    )
                );

                $myChart->series[] = array(
                    'name' => 'London',
                    'data' => array(
                        48.9,
                        38.8,
                        39.3,
                        41.4,
                        47.0,
                        48.3,
                        59.0,
                        59.6,
                        52.4,
                        65.2,
                        59.3,
                        51.2
                    )
                );

                $myChart->series[] = array(
                    'name' => 'Berlin',
                    'data' => array(
                        42.4,
                        33.2,
                        34.5,
                        39.7,
                        52.6,
                        75.5,
                        57.4,
                        60.4,
                        47.6,
                        39.1,
                        46.8,
                        51.1
                    )
                );

                $this->set(compact('myChart', 'chartName'));
        }

        public function line() {
                
                $chartName = 'Line Chart';

                $myChart = $this->Highcharts->createChart();
                
                $myChart->chart = array(
                    'renderTo' => 'linewrapper',
                    'type' => 'line',
                    'marginRight' => 130,
                    'marginBottom' => 25
                );

                $myChart->title = array(
                    'text' => 'Monthly Average Temperature',
                    'x' => - 20
                );
                
                $myChart->subtitle = array(
                    'text' => 'Source: WorldClimate.com',
                    'x' => - 20
                );

                $myChart->xAxis->categories = array(
                    'Jan',
                    'Feb',
                    'Mar',
                    'Apr',
                    'May',
                    'Jun',
                    'Jul',
                    'Aug',
                    'Sep',
                    'Oct',
                    'Nov',
                    'Dec'
                );

                $myChart->yAxis = array(
                    'title' => array(
                        'text' => 'Temperature (°C)'
                    ),
                    'plotLines' => array(
                        array(
                            'value' => 0,
                            'width' => 1,
                            'color' => '#808080'
                        )
                    )
                );
                $myChart->legend = array(
                    'layout' => 'vertical',
                    'align' => 'right',
                    'verticalAlign' => 'top',
                    'x' => - 10,
                    'y' => 100,
                    'borderWidth' => 0
                );

                $myChart->series[] = array(
                    'name' => 'Tokyo',
                    'data' => array(
                        7.0,
                        6.9,
                        9.5,
                        14.5,
                        18.2,
                        21.5,
                        25.2,
                        26.5,
                        23.3,
                        18.3,
                        13.9,
                        9.6
                    )
                );
                
                $myChart->series[] = array(
                    'name' => 'New York',
                    'data' => array(
                        - 0.2,
                        0.8,
                        5.7,
                        11.3,
                        17.0,
                        22.0,
                        24.8,
                        24.1,
                        20.1,
                        14.1,
                        8.6,
                        2.5
                     )
                );
                
                $myChart->series[] = array(
                    'name' => 'Berlin',
                    'data' => array(
                        - 0.9,
                        0.6,
                        3.5,
                        8.4,
                        13.5,
                        17.0,
                        18.6,
                        17.9,
                        14.3,
                        9.0,
                        3.9,
                        1.0
                    )
                );
                
                $myChart->series[] = array(
                    'name' => 'London',
                    'data' => array(
                        3.9,
                        4.2,
                        5.7,
                        8.5,
                        11.9,
                        15.2,
                        17.0,
                        16.6,
                        14.2,
                        10.3,
                        6.6,
                        4.8
                    )
                );

                $myChart->tooltip->formatter = $this->Highcharts->createJsExpr(
                "function() { return '<b>'+ this.series.name +'</b><br/>'+ this.x +': '+ this.y +'°C';}");

                
                $this->set(compact('myChart', 'chartName'));
        }

        public function donut() {
                
                $browserData = array(
                    array(
                        'color' => '#7cb5ec',
                        'name' => 'MSIE',
                        'y' => 55.11
                    ),
                    array(
                        'color' => '#434348',
                        'name' => 'Firefox',
                        'y' => 21.63
                    ),
                    array(
                        'color' => '#90ed7d',
                        'name' => 'Chrome',
                        'y' => 11.94
                    ),
                    array(
                        'color' => '#f7a35c',
                        'name' => 'Safari',
                        'y' => 7.15
                    ),
                    array(
                        'color' => '#8085e9',
                        'name' => 'Opera',
                        'y' => 2.14
                    )
                );
                
                $versionsData = array(
                    array(
                        'name' => 'MSIE 6.0',
                        'y' => 10.85,
                        'color' => 'rgba(175,232,255,1)'
                    ),
                    array(
                        'name' => 'MSIE 7.0',
                        'y' => 7.35,
                        'color' => 'rgba(162,219,255,1)'
                    ),
                    array(
                        'name' => 'MSIE 8.0',
                        'y' => 33.06,
                        'color' => 'rgba(149,206,255,1)'
                    ),
                    array(
                        'name' => 'MSIE 9.0',
                        'y' => 2.81,
                        'color' => 'rgba(136,193,248,1)'
                    ),
                    array(
                        'name' => 'Firefox 2.0',
                        'y' => 0.2,
                        'color' => 'rgba(118,118,123,1)'
                    ),
                    array(
                        'name' => 'Firefox 3.0',
                        'y' => 0.83,
                        'color' => 'rgba(107,107,112,1)'
                    ),
                    array(
                        'name' => 'Firefox 3.5',
                        'y' => 1.58,
                        'color' => 'rgba(97,97,102,1)'
                    ),
                    array(
                        'name' => 'Firefox 3.6',
                        'y' => 13.12,
                        'color' => 'rgba(87,87,92,1)'
                    ),
                    array(
                        'name' => 'Firefox 4.0',
                        'y' => 5.43,
                        'color' => 'rgba(77,77,82,1)'
                    ),
                    array(
                        'name' => 'Chrome 5.0',
                        'y' => 0.12,
                        'color' => 'rgba(195,255,176,1)'
                    ),
                    array(
                        'name' => 'Chrome 6.0',
                        'y' => 0.19,
                        'color' => 'rgba(188,255,169,1)'
                    ),
                    array(
                        'name' => 'Chrome 7.0',
                        'y' => 0.12,
                        'color' => 'rgba(182,255,163,1)'
                    ),
                    array(
                        'name' => 'Chrome 8.0',
                        'y' => 0.36,
                        'color' => 'rgba(175,255,156,1)'
                    ),
                    array(
                        'name' => 'Chrome 9.0',
                        'y' => 0.32,
                        'color' => 'rgba(169,255,150,1)'
                    ),
                    array(
                        'name' => 'Chrome 10.0',
                        'y' => 9.91,
                        'color' => 'rgba(163,255,144,1)'
                    ),
                    array(
                        'name' => 'Chrome 11.0',
                        'y' => 0.5,
                        'color' => 'rgba(156,249,137,1)'
                    ),
                    array(
                        'name' => 'Chrome 12.0',
                        'y' => 0.22,
                        'color' => 'rgba(150,243,131,1)'
                    ),
                    array(
                        'name' => 'Safari 5.0',
                        'y' => 4.55,
                        'color' => 'rgba(255,214,143,1)'
                    ),
                    array(
                        'name' => 'Safari 4.0',
                        'y' => 1.42,
                        'color' => 'rgba(255,206,135,1)'
                    ),
                    array(
                        'name' => 'Safari Win 5.0',
                        'y' => 0.23,
                        'color' => 'rgba(255,199,128,1)'
                    ),
                    array(
                        'name' => 'Safari 4.1',
                        'y' => 0.21,
                        'color' => 'rgba(255,192,121,1)'
                    ),
                    array(
                        'name' => 'Safari/Maxthon',
                        'y' => 0.2,
                        'color' => 'rgba(255,184,113,1)'
                    ),
                    array(
                        'name' => 'Safari 3.1',
                        'y' => 0.19,
                        'color' => 'rgba(255,177,106,1)'
                    ),
                    array(
                        'name' => 'Safari 4.1',
                        'y' => 0.14,
                        'color' => 'rgba(254,170,99,1)'
                    ),
                    array(
                        'name' => 'Opera 9.x',
                        'y' => 0.12,
                        'color' => 'rgba(179,184,255,1)'
                    ),
                   array(
                        'name' => 'Opera 10.x',
                        'y' => 0.37,
                        'color' => 'rgba(162,167,255,1)'
                    ),
                    array(
                        'name' => 'Opera 11.x',
                        'y' => 1.65,
                        'color' => 'rgba(145,150,250,1)'
                    )
                );
                
                $chartName = 'Donut Chart';
                $myChart = $this->Highcharts->createChart();

                $myChart->chart->renderTo = 'donutwrapper';
                $myChart->chart->type = 'pie';
                $myChart->title->text = 'Browser Market Share, April, 2011';
                $myChart->yAxis->title->text = 'Total percent market share';
                $myChart->plotOptions->pie->shadow = true;

                $myChart->tooltip->formatter = $this->Highcharts->createJsExpr(
                    "function() {
                    return '<b>'+ this.point.name +'</b>: '+ this.y +' %'; }");

                $myChart->series[] = array(
                    'name' => 'Browsers',
                    'data' => $browserData,
                    'size' => '60%',
                    'dataLabels' => array(
                        'formatter' => $this->Highcharts->createJsExpr("function() {
                    return this.y > 5 ? this.point.name : null; }"),
                        'color' => 'white',
                        'distance' => - 30
                    )
                );

                $myChart->series[1]->name = 'Versions';
                $myChart->series[1]->data = $versionsData;
                $myChart->series[1]->innerSize = '60%';

                $myChart->series[1]->dataLabels->formatter = $this->Highcharts->createJsExpr(
                    "function() {
                    return this.y > 1 ? '<b>'+ this.point.name +':</b> '+ this.y +'%'  : null;}");

                // We can also use Highchart library to produce any kind of javascript structures
                $chartData = $this->Highcharts->createChart();
                $chartData[0]->y = 55.11;
                $chartData[0]->color = $this->Highcharts->createJsExpr('colors[0]');
                $chartData[0]->drilldown->name = 'MSIE versions';
                $chartData[0]->drilldown->categories = array(
                    'MSIE 6.0',
                    'MSIE 7.0',
                    'MSIE 8.0',
                    'MSIE 9.0'
                );
                $chartData[0]->drilldown->data = array(
                    10.85,
                    7.35,
                    33.06,
                    2.81
                );
                $chartData[0]->drilldown->color = $this->Highcharts->createJsExpr('colors[0]');

                $chartData[1]->y = 21.63;
                $chartData[1]->color = $this->Highcharts->createJsExpr('colors[1]');
                $chartData[1]->drilldown->name = 'Firefox versions';

                $chartData[1]->drilldown->categories = array(
                    'Firefox 2.0',
                    'Firefox 3.0',
                    'Firefox 3.5',
                    'Firefox 3.6',
                    'Firefox 4.0'
                );

                $chartData[1]->drilldown->data = array(
                    0.20,
                    0.83,
                    1.58,
                    13.12,
                    5.43
                );
                $chartData[1]->drilldown->color = $this->Highcharts->createJsExpr('colors[1]');

                $chartData[2]->y = 11.94;
                $chartData[2]->color = $this->Highcharts->createJsExpr('colors[2]');
                $chartData[2]->drilldown->name = 'Chrome versions';

                $chartData[2]->drilldown->categories = array(
                    'Chrome 5.0',
                    'Chrome 6.0',
                    'Chrome 7.0',
                    'Chrome 8.0',
                    'Chrome 9.0',
                    'Chrome 10.0',
                    'Chrome 11.0',
                    'Chrome 12.0'
                );

                $chartData[2]->drilldown->data = array(
                    0.12,
                    0.19,
                    0.12,
                    0.36,
                    0.32,
                    9.91,
                    0.50,
                    0.22
                );
                $chartData[2]->drilldown->color = $this->Highcharts->createJsExpr('colors[2]');

                $chartData[3]->y = 7.15;
                $chartData[3]->color = $this->Highcharts->createJsExpr('colors[3]');
                $chartData[3]->drilldown->name = 'Safari versions';

                $chartData[3]->drilldown->categories = array(
                    'Safari 5.0',
                    'Safari 4.0',
                    'Safari Win 5.0',
                    'Safari 4.1',
                    'Safari/Maxthon',
                    'Safari 3.1',
                    'Safari 4.1'
                );

                $chartData[3]->drilldown->data = array(
                    4.55,
                    1.42,
                    0.23,
                    0.21,
                    0.20,
                    0.19,
                    0.14
                );
                $chartData[3]->drilldown->color = $this->Highcharts->createJsExpr('colors[3]');

                $chartData[4]->y = 2.14;
                $chartData[4]->color = $this->Highcharts->createJsExpr('colors[4]');
                $chartData[4]->drilldown->name = 'Opera versions';
                $chartData[4]->drilldown->categories = array(
                    'Opera 9.x',
                    'Opera 10.x',
                    'Opera 11.x'
                );
                $chartData[4]->drilldown->data = array(
                    0.12,
                    0.37,
                    1.65
                );
                $chartData[4]->drilldown->color = $this->Highcharts->createJsExpr('colors[4]');

                $this->set(compact('myChart', 'chartName'));
        }

        public function pie_drill_down() {

                $renderTo = 'pie_drill_down';

                $colors = array('#4572A7', '#AA4643', '#89A54E', '#80699B', '#3D96AE');  // custom list of colours
                
                $chartData = array(
                    array(
                        'y' => 55.11,
                        'color' => $colors[0],
                        'name' => 'Internet Explorer',
                        'drilldown' => 'msie'
                    ),
                    array(
                        'y' => 21.63,
                        'color' => $colors[1],
                        'name' => 'Mozilla Firefox',
                        'drilldown' => 'firefox'
                    ),
                    array(
                        'y' => 11.94,
                        'name' => 'Google Chrome',
                        'color' => $colors[2],
                        'drilldown' => 'chrome'
                    ),
                    array(
                        'y' => 7.15,
                        'name' => 'Safari',
                        'color' => $colors[3],
                        'drilldown' => 'safari'
                    ),
                    array(
                        'y' => 2.14,
                        'name' => 'Opera',
                        'color' => $colors[4],
                        'drilldown' => 'opera'
                    )
                );

                $drillDownData = array(
                    array(
                        'name' => 'MSIE Versions',
                        'id' => 'msie',
                        'data' => array(
                            array('MSIE 6.0', 10.85),
                            array('MSIE 7.0', 7.35),
                            array('MSIE 8.0', 33.06),
                            array('MSIE 9.0', 2.81),
                        )
                    ),
                    array(
                        'name' => 'Firefox Versions',
                        'id' => 'firefox',
                        'data' => array(
                            array('Firefox 2.0', 0.20),
                            array('Firefox 3.0', 0.83),
                            array('Firefox 3.5', 1.58),
                            array('Firefox 3.6', 13.12),
                            array('Firefox 4.0', 5.43),
                        )
                    ),
                    array(
                        'name' => 'Chrome Versions',
                        'id' => 'chrome',
                        'data' => array(
                            array('Chrome 5.0', 0.12),
                            array('Chrome 6.0', 0.19),
                            array('Chrome 7.0', 0.12),
                            array('Chrome 8.0', 0.36),
                            array('Chrome 9.0', 0.32,),
                            array('Chrome 10.0', 9.91),
                            array('Chrome 11.0', 0.50),
                            array('Chrome 12.0', 0.22),
                        )
                    ),
                    array(
                        'name' => 'Safari Versions',
                        'id' => 'safari',
                        'data' => array(
                            array('Safari 5.0', 4.55),
                            array('Safari 4.0', 1.42),
                            array('Safari Win 5.0', 0.23),
                            array('Safari 4.1', 0.21),
                            array('Safari-Maxthon', 0.20),
                            array('Safari 3.1', 0.19),
                            array('Safari 4.1', 0.14),
                        )
                    ),
                    array(
                        'name' => 'Opera Versions',
                        'id' => 'opera',
                        'data' => array(
                            array('Opera 9.x', 0.12),
                            array('Opera 10.x', 0.37),
                            array('Opera 11.x', 1.65),
                        )
                    )
                );

                $chartName = 'Pie Chart';

                $pieChart = $this->Highcharts->createChart();

                $this->Highcharts->setChartParams($chartName, array(
                    'renderTo' => 'piewrapper', // div to display chart inside
                    'chartWidth' => 1024,
                    'chartHeight' => 768,
                    'chartMarginTop' => 60,
                    'chartMarginLeft' => 90,
                    'chartMarginRight' => 30,
                    'chartMarginBottom' => 110,
                    'chartSpacingRight' => 10,
                    'chartSpacingBottom' => 15,
                    'chartSpacingLeft' => 0,
                    'chartAlignTicks' => FALSE,
                    'chartBackgroundColorLinearGradient' => array(0, 0, 0, 300),
                    'chartBackgroundColorStops' => array(array(0, 'rgb(217, 217, 217)'), array(1, 'rgb(255, 255, 255)')),
                    'title' => 'Browser Usage Statistics',
                    'titleAlign' => 'left',
                    'titleFloating' => TRUE,
                    'titleStyleFont' => '18px Metrophobic, Arial, sans-serif',
                    'titleStyleColor' => '#0099ff',
                    'titleX' => 20,
                    'titleY' => 20,
                    'legendEnabled' => TRUE,
                    'legendLayout' => 'horizontal',
                    'legendAlign' => 'center',
                    'legendVerticalAlign ' => 'bottom',
                    'legendItemStyle' => array('color' => '#222'),
                    'legendBackgroundColorLinearGradient' => array(0, 0, 0, 25),
                    'legendBackgroundColorStops' => array(array(0, 'rgb(217, 217, 217)'), array(1, 'rgb(255, 255, 255)')),
                    'tooltipEnabled' => TRUE,
                    'tooltipBackgroundColorLinearGradient' => array(0, 0, 0, 50), // triggers js error
                    'tooltipBackgroundColorStops' => array(array(0, 'rgb(217, 217, 217)'), array(1, 'rgb(255, 255, 255)')),
                        )
                );

                $series = $this->Highcharts->addChartSeries();

                $series->addName('Brands')
                        ->addData($chartData);

                $pieChart->addSeries($series);

                $pieChart->addDrillDownData($drillDownData);

                $this->set(compact('myChart', 'pieChart'));
        }

        public function pie() {
                
                $pieData = array(
                    array(
                        'name' => 'Chrome',
                        'y' => 45.0,
                        'sliced' => true,
                        'selected' => true
                    ),
                    array('IE', 26.8),
                    array('Firefox', 12.8),
                    array('Safari', 8.5),
                    array('Opera', 6.2),
                    array('Others', 0.7)
                );

                $chartName = 'Pie Chart';

                $pieChart = $this->Highcharts->createChart();

                $pieChart->chart->renderTo = 'piewrapper';
                $pieChart->chart->plotBackgroundColor = null;
                $pieChart->chart->plotBorderWidth = null;
                $pieChart->chart->plotShadow = false;
                $pieChart->title->text = 'Browser market shares at a specific website, 2010';

                $pieChart->tooltip->formatter = $this->Highcharts->createJsExpr(
                    "function() {
                    return '<b>'+ this.point.name +'</b>: '+ this.percentage +' %';}");

                $pieChart->plotOptions->pie->allowPointSelect = 1;
                $pieChart->plotOptions->pie->cursor = 'pointer';
                $pieChart->plotOptions->pie->dataLabels->enabled = 1;
                $pieChart->plotOptions->pie->dataLabels->color = '#000000';
                $pieChart->plotOptions->pie->dataLabels->connectorColor = '#000000';

                $pieChart->plotOptions->pie->dataLabels->formatter = $this->Highcharts->createJsExpr(
                    "function() {
                    return '<b>'+ this.point.name +'</b>: '+ this.percentage +' %'; }");

                $pieChart->series[] = array(
                    'type' => 'pie',
                    'name' => 'Browser share',
                    'data' => array(
                        array(
                            'Firefox',
                            45
                        ),
                        array(
                            'IE',
                            26.8
                        ),
                        array(
                            'name' => 'Chrome',
                            'y' => 12.8,
                            'sliced' => true,
                            'selected' => true
                        ),
                        array(
                            'Safari',
                            8.5
                        ),
                        array(
                            'Opera',
                            6.2
                        ),
                        array(
                            'Others',
                            0.7
                        )
                    )
                );

                $this->set(compact('pieChart', 'chartName'));
        }

        public function scatter() {
                
                $chartName = 'Scatter Chart';                
                $myChart = $this->Highcharts->createChart();
                $myChart->chart->renderTo = 'scatterwrapper';
                $myChart->chart->type = 'scatter';
                $myChart->chart->zoomType = 'xy';
                $myChart->title->text = 'Height Versus Weight of 260 Females';
                $myChart->subtitle->text = 'Source: Heinz 2003';
                $myChart->xAxis->title->enabled = 1;
                $myChart->xAxis->title->text = 'Height (cm)';
                $myChart->xAxis->startOnTick = 1;
                $myChart->xAxis->endOnTick = 1;
                $myChart->xAxis->showLastLabel = 1;
                $myChart->yAxis->title->text = 'Weight (kg)';
                $myChart->tooltip->formatter = $this->Highcharts->createJsExpr(
                    "function() {
                        return ''+
                        this.x +' cm, '+ this.y +' kg';
                    }"
                );
                $myChart->legend->layout = 'vertical';
                $myChart->legend->align = 'left';
                $myChart->legend->verticalAlign = 'top';
                $myChart->legend->x = 100;
                $myChart->legend->y = 70;
                $myChart->legend->floating = 1;
                $myChart->legend->backgroundColor = '#FFFFFF';
                $myChart->legend->borderWidth = 1;
                $myChart->plotOptions->scatter->marker->radius = 5;
                $myChart->plotOptions->scatter->marker->states->hover->enabled = 1;
                $myChart->plotOptions->scatter->marker->states->hover->lineColor = 'rgb(100,100,100)';
                $myChart->plotOptions->scatter->states->hover->marker->enabled = false;
                $myChart->series[] = array(
                    'name' => 'Female',
                    'color' => 'rgba(223, 83, 83, .5)',
                    'data' => array(
                        array(
                            161.2,
                            51.6
                        ),
                        array(
                            167.5,
                            59.0
                        ),
                        array(
                            159.5,
                            49.2
                        ),
                        array(
                            157.0,
                            63.0
                        ),
                        array(
                            155.8,
                            53.6
                        ),
                        array(
                            170.0,
                            59.0
                        ),
                        array(
                            159.1,
                            47.6
                        ),
                        array(
                            166.0,
                            69.8
                        ),
                        array(
                            176.2,
                            66.8
                        ),
                        array(
                            160.2,
                            75.2
                        ),
                        array(
                            172.5,
                            55.2
                        ),
                        array(
                            170.9,
                            54.2
                        ),
                        array(
                            172.9,
                            62.5
                        ),
                        array(
                            153.4,
                            42.0
                        ),
                        array(
                            160.0,
                            50.0
                        ),
                        array(
                            147.2,
                            49.8
                        ),
                        array(
                            168.2,
                            49.2
                        ),
                        array(
                            175.0,
                            73.2
                        ),
                        array(
                            157.0,
                            47.8
                        ),
                        array(
                            167.6,
                            68.8
                        ),
                        array(
                            159.5,
                            50.6
                        ),
                        array(
                            175.0,
                            82.5
                        ),
                        array(
                            166.8,
                            57.2
                        ),
                        array(
                            176.5,
                            87.8
                        ),
                        array(
                            170.2,
                            72.8
                        ),
                        array(
                            174.0,
                            54.5
                        ),
                        array(
                            173.0,
                            59.8
                        ),
                        array(
                            179.9,
                            67.3
                        ),
                        array(
                            170.5,
                            67.8
                        ),
                        array(
                            160.0,
                            47.0
                        ),
                        array(
                            154.4,
                            46.2
                        ),
                        array(
                            162.0,
                            55.0
                        ),
                        array(
                            176.5,
                            83.0
                        ),
                        array(
                            160.0,
                            54.4
                        ),
                        array(
                            152.0,
                            45.8
                        ),
                        array(
                            162.1,
                            53.6
                        ),
                        array(
                            170.0,
                            73.2
                        ),
                        array(
                            160.2,
                            52.1
                        ),
                        array(
                            161.3,
                            67.9
                        ),
                        array(
                            166.4,
                            56.6
                        ),
                        array(
                            168.9,
                            62.3
                        ),
                        array(
                            163.8,
                            58.5
                        ),
                        array(
                            167.6,
                            54.5
                        ),
                        array(
                            160.0,
                            50.2
                        ),
                        array(
                            161.3,
                            60.3
                        ),
                        array(
                            167.6,
                            58.3
                        ),
                        array(
                            165.1,
                            56.2
                        ),
                        array(
                            160.0,
                            50.2
                        ),
                        array(
                            170.0,
                            72.9
                        ),
                        array(
                            157.5,
                            59.8
                        ),
                        array(
                            167.6,
                            61.0
                        ),
                        array(
                            160.7,
                            69.1
                        ),
                        array(
                            163.2,
                            55.9
                        ),
                        array(
                            152.4,
                            46.5
                        ),
                        array(
                            157.5,
                            54.3
                        ),
                        array(
                            168.3,
                            54.8
                        ),
                        array(
                            180.3,
                            60.7
                        ),
                        array(
                            165.5,
                            60.0
                        ),
                        array(
                            165.0,
                            62.0
                        ),
                        array(
                            164.5,
                            60.3
                        ),
                        array(
                            156.0,
                            52.7
                        ),
                        array(
                            160.0,
                            74.3
                        ),
                        array(
                            163.0,
                            62.0
                        ),
                        array(
                            165.7,
                            73.1
                        ),
                        array(
                            161.0,
                            80.0
                        ),
                        array(
                            162.0,
                            54.7
                        ),
                        array(
                            166.0,
                            53.2
                        ),
                        array(
                            174.0,
                            75.7
                        ),
                        array(
                            172.7,
                            61.1
                        ),
                        array(
                            167.6,
                            55.7
                        ),
                        array(
                            151.1,
                            48.7
                        ),
                        array(
                            164.5,
                            52.3
                        ),
                        array(
                            163.5,
                            50.0
                        ),
                        array(
                            152.0,
                            59.3
                        ),
                        array(
                            169.0,
                            62.5
                        ),
                        array(
                            164.0,
                            55.7
                        ),
                        array(
                            161.2,
                            54.8
                        ),
                        array(
                            155.0,
                            45.9
                        ),
                        array(
                            170.0,
                            70.6
                        ),
                        array(
                            176.2,
                            67.2
                        ),
                        array(
                            170.0,
                            69.4
                        ),
                        array(
                            162.5,
                            58.2
                        ),
                        array(
                            170.3,
                            64.8
                        ),
                        array(
                            164.1,
                            71.6
                        ),
                        array(
                            169.5,
                            52.8
                        ),
                        array(
                            163.2,
                            59.8
                        ),
                        array(
                            154.5,
                            49.0
                        ),
                        array(
                            159.8,
                            50.0
                        ),
                        array(
                            173.2,
                            69.2
                        ),
                        array(
                            170.0,
                            55.9
                        ),
                        array(
                            161.4,
                            63.4
                        ),
                        array(
                            169.0,
                            58.2
                        ),
                        array(
                            166.2,
                            58.6
                        ),
                        array(
                            159.4,
                            45.7
                        ),
                        array(
                            162.5,
                            52.2
                        ),
                        array(
                            159.0,
                            48.6
                        ),
                        array(
                            162.8,
                            57.8
                        ),
                        array(
                            159.0,
                            55.6
                        ),
                        array(
                            179.8,
                            66.8
                        ),
                        array(
                            162.9,
                            59.4
                        ),
                        array(
                            161.0,
                            53.6
                        ),
                        array(
                            151.1,
                            73.2
                        ),
                        array(
                            168.2,
                            53.4
                        ),
                        array(
                            168.9,
                            69.0
                        ),
                        array(
                            173.2,
                            58.4
                        ),
                        array(
                            171.8,
                            56.2
                        ),
                        array(
                            178.0,
                            70.6
                        ),
                        array(
                            164.3,
                            59.8
                        ),
                        array(
                            163.0,
                            72.0
                        ),
                        array(
                            168.5,
                            65.2
                        ),
                        array(
                            166.8,
                            56.6
                        ),
                        array(
                            172.7,
                            105.2
                        ),
                        array(
                            163.5,
                            51.8
                        ),
                        array(
                            169.4,
                            63.4
                        ),
                        array(
                            167.8,
                            59.0
                        ),
                        array(
                            159.5,
                            47.6
                        ),
                        array(
                            167.6,
                            63.0
                        ),
                        array(
                            161.2,
                            55.2
                        ),
                        array(
                            160.0,
                            45.0
                        ),
                        array(
                            163.2,
                            54.0
                        ),
                        array(
                            162.2,
                            50.2
                        ),
                        array(
                            161.3,
                            60.2
                        ),
                        array(
                            149.5,
                            44.8
                        ),
                        array(
                            157.5,
                            58.8
                        ),
                        array(
                            163.2,
                            56.4
                        ),
                        array(
                            172.7,
                            62.0
                        ),
                        array(
                            155.0,
                            49.2
                        ),
                        array(
                            156.5,
                            67.2
                        ),
                        array(
                            164.0,
                            53.8
                        ),
                        array(
                            160.9,
                            54.4
                        ),
                        array(
                            162.8,
                            58.0
                        ),
                        array(
                            167.0,
                            59.8
                        ),
                        array(
                            160.0,
                            54.8
                        ),
                        array(
                            160.0,
                            43.2
                        ),
                        array(
                            168.9,
                            60.5
                        ),
                        array(
                            158.2,
                            46.4
                        ),
                        array(
                            156.0,
                            64.4
                        ),
                        array(
                            160.0,
                            48.8
                        ),
                        array(
                            167.1,
                            62.2
                        ),
                        array(
                            158.0,
                            55.5
                        ),
                        array(
                            167.6,
                            57.8
                        ),
                        array(
                            156.0,
                            54.6
                        ),
                        array(
                            162.1,
                            59.2
                        ),
                        array(
                            173.4,
                            52.7
                        ),
                        array(
                            159.8,
                            53.2
                        ),
                        array(
                            170.5,
                            64.5
                        ),
                        array(
                            159.2,
                            51.8
                        ),
                        array(
                            157.5,
                            56.0
                        ),
                        array(
                            161.3,
                            63.6
                        ),
                        array(
                            162.6,
                            63.2
                        ),
                        array(
                            160.0,
                            59.5
                        ),
                        array(
                            168.9,
                            56.8
                        ),
                        array(
                            165.1,
                            64.1
                        ),
                        array(
                            162.6,
                            50.0
                        ),
                        array(
                            165.1,
                            72.3
                        ),
                        array(
                            166.4,
                            55.0
                        ),
                        array(
                            160.0,
                            55.9
                        ),
                        array(
                            152.4,
                            60.4
                        ),
                        array(
                            170.2,
                            69.1
                        ),
                        array(
                            162.6,
                            84.5
                        ),
                        array(
                            170.2,
                            55.9
                        ),
                        array(
                            158.8,
                            55.5
                        ),
                        array(
                            172.7,
                            69.5
                        ),
                        array(
                            167.6,
                            76.4
                        ),
                        array(
                            162.6,
                            61.4
                        ),
                        array(
                            167.6,
                            65.9
                        ),
                        array(
                            156.2,
                            58.6
                        ),
                        array(
                            175.2,
                            66.8
                        ),
                        array(
                            172.1,
                            56.6
                        ),
                        array(
                            162.6,
                            58.6
                        ),
                        array(
                            160.0,
                            55.9
                        ),
                        array(
                            165.1,
                            59.1
                        ),
                        array(
                            182.9,
                            81.8
                        ),
                        array(
                            166.4,
                            70.7
                        ),
                        array(
                            165.1,
                            56.8
                        ),
                        array(
                            177.8,
                            60.0
                        ),
                        array(
                            165.1,
                            58.2
                        ),
                        array(
                            175.3,
                            72.7
                        ),
                        array(
                            154.9,
                            54.1
                        ),
                        array(
                            158.8,
                            49.1
                        ),
                        array(
                            172.7,
                            75.9
                        ),
                        array(
                            168.9,
                            55.0
                        ),
                        array(
                            161.3,
                            57.3
                        ),
                        array(
                            167.6,
                            55.0
                        ),
                        array(
                            165.1,
                            65.5
                        ),
                        array(
                            175.3,
                            65.5
                        ),
                        array(
                            157.5,
                            48.6
                        ),
                        array(
                            163.8,
                            58.6
                        ),
                        array(
                            167.6,
                            63.6
                        ),
                        array(
                            165.1,
                            55.2
                        ),
                        array(
                            165.1,
                            62.7
                        ),
                        array(
                            168.9,
                            56.6
                        ),
                        array(
                            162.6,
                            53.9
                        ),
                        array(
                            164.5,
                            63.2
                        ),
                        array(
                            176.5,
                            73.6
                        ),
                        array(
                            168.9,
                            62.0
                        ),
                        array(
                            175.3,
                            63.6
                        ),
                        array(
                            159.4,
                            53.2
                        ),
                        array(
                            160.0,
                            53.4
                        ),
                        array(
                            170.2,
                            55.0
                        ),
                        array(
                            162.6,
                            70.5
                        ),
                        array(
                            167.6,
                            54.5
                        ),
                        array(
                            162.6,
                            54.5
                        ),
                        array(
                            160.7,
                            55.9
                        ),
                        array(
                            160.0,
                            59.0
                        ),
                        array(
                            157.5,
                            63.6
                        ),
                        array(
                            162.6,
                            54.5
                        ),
                        array(
                            152.4,
                            47.3
                        ),
                        array(
                            170.2,
                            67.7
                        ),
                        array(
                            165.1,
                            80.9
                        ),
                        array(
                            172.7,
                            70.5
                        ),
                        array(
                            165.1,
                            60.9
                        ),
                        array(
                            170.2,
                            63.6
                        ),
                        array(
                            170.2,
                            54.5
                        ),
                        array(
                            170.2,
                            59.1
                        ),
                        array(
                            161.3,
                            70.5
                        ),
                        array(
                            167.6,
                            52.7
                        ),
                        array(
                            167.6,
                            62.7
                        ),
                        array(
                            165.1,
                            86.3
                        ),
                        array(
                            162.6,
                            66.4
                        ),
                        array(
                            152.4,
                            67.3
                        ),
                        array(
                            168.9,
                            63.0
                        ),
                        array(
                            170.2,
                            73.6
                        ),
                        array(
                            175.2,
                            62.3
                        ),
                        array(
                            175.2,
                            57.7
                        ),
                        array(
                            160.0,
                            55.4
                        ),
                        array(
                            165.1,
                            104.1
                        ),
                        array(
                            174.0,
                            55.5
                        ),
                        array(
                            170.2,
                            77.3
                        ),
                        array(
                            160.0,
                            80.5
                        ),
                        array(
                            167.6,
                            64.5
                        ),
                        array(
                            167.6,
                            72.3
                        ),
                        array(
                            167.6,
                            61.4
                        ),
                        array(
                            154.9,
                            58.2
                        ),
                        array(
                            162.6,
                            81.8
                        ),
                        array(
                            175.3,
                            63.6
                        ),
                        array(
                            171.4,
                            53.4
                        ),
                        array(
                            157.5,
                            54.5
                        ),
                        array(
                            165.1,
                            53.6
                        ),
                        array(
                            160.0,
                            60.0
                        ),
                        array(
                            174.0,
                            73.6
                        ),
                        array(
                            162.6,
                            61.4
                        ),
                        array(
                            174.0,
                            55.5
                        ),
                        array(
                            162.6,
                            63.6
                        ),
                        array(
                            161.3,
                            60.9
                        ),
                        array(
                            156.2,
                            60.0
                        ),
                        array(
                            149.9,
                            46.8
                        ),
                        array(
                            169.5,
                            57.3
                        ),
                        array(
                            160.0,
                            64.1
                        ),
                        array(
                            175.3,
                            63.6
                        ),
                        array(
                            169.5,
                            67.3
                        ),
                        array(
                            160.0,
                            75.5
                        ),
                        array(
                            172.7,
                            68.2
                        ),
                        array(
                            162.6,
                            61.4
                        ),
                        array(
                            157.5,
                            76.8
                        ),
                        array(
                            176.5,
                            71.8
                        ),
                        array(
                            164.4,
                            55.5
                        ),
                        array(
                            160.7,
                            48.6
                        ),
                        array(
                            174.0,
                            66.4
                        ),
                        array(
                            163.8,
                            67.3
                        )
                    )
                );

                $this->set(compact('myChart', 'chartName'));
        }

        public function spline() {

                $chartName = 'Spline Chart';

                $myChart = $this->Highcharts->createChart();


                $myChart->chart->renderTo = 'splinewrapper';
                $myChart->chart->type = 'spline';
                $myChart->title->text = 'Average fruit consumption during one week';
                $myChart->legend->layout = 'vertical';
                $myChart->legend->align = 'left';
                $myChart->legend->verticalAlign = 'top';
                $myChart->legend->x = 150;
                $myChart->legend->y = 100;
                $myChart->legend->floating = 1;
                $myChart->legend->borderWidth = 1;
                $myChart->legend->backgroundColor = '#FFFFFF';
                $myChart->xAxis->categories = array(
                    'Monday',
                    'Tuesday',
                    'Wednesday',
                    'Thursday',
                    'Friday',
                    'Saturday',
                    'Sunday'
                );
                $myChart->yAxis->title->text = 'Fruit units';
                $myChart->tooltip->formatter = $this->Highcharts->createJsExpr("function() { return ''+ this.x +': '+ this.y +' units'; }");
                $myChart->credits->enabled = true;
                $myChart->plotOptions->areaspline->fillOpacity = 0.5;
                $myChart->series[] = array(
                    'name' => 'John',
                    'data' => array(
                        3,
                        4,
                        3,
                        5,
                        4,
                        10,
                        12
                    )
                );
                $myChart->series[] = array(
                    'name' => 'Jane',
                    'data' => array(
                        1,
                        3,
                        4,
                        3,
                        3,
                        5,
                        4
                    )
                );
                
                $this->set(compact('myChart', 'chartName'));
        }
        
        public function bubble() {
                $chartName = 'Bubble Chart';
                $myChart = $this->Highcharts->createChart();
                $myChart->chart->renderTo = 'bubblewrapper';
                $myChart->chart->type = 'bubble';
                $myChart->chart->zoomType = 'xy';
                $myChart->title->text = 'Highcharts Bubbles';

                $myChart->series = array(
                    array(
                        'name' => 'Bubble Series',
                        'data' => array(
                            array(97, 36, 79),
                            array(94, 74, 60),
                            array(68, 76, 58),
                            array(64, 87, 56),
                            array(68, 27, 73),
                            array(74, 99, 42),
                            array(7, 93, 87),
                            array(51, 69, 40),
                            array(38, 23, 33),
                            array(57, 86, 31),
                            array(25, 10, 87),
                            array(2, 75, 59),
                            array(11, 54, 8),
                            array(86, 55, 93),
                            array(5, 3, 58),
                            array(90, 63, 44),
                            array(91, 33, 17),
                            array(97, 3, 56),
                            array(15, 67, 48),
                            array(54, 25, 81),
                            array(47, 47, 21),
                            array(20, 12, 4),
                            array(6, 76, 91),
                            array(38, 30, 60),
                            array(57, 98, 64),
                            array(61, 17, 80),
                            array(83, 60, 13),
                            array(67, 78, 75),
                            array(64, 12, 10),
                            array(30, 77, 82)
                        )
                    )
                );
                
                $this->set(compact('myChart', 'chartName'));
        }

}
