<?php

/**
 *  CakePHP Highcharts Plugin
 *
 * 	Copyright (C) 2015 Kurn La Montagne / destinydriven
 * 	<https://github.com/destinydriven>
 *
 * 	Multi-licensed under:
 * 		MPL <http://www.mozilla.org/MPL/MPL-1.1.html>
 * 		LGPL <http://www.gnu.org/licenses/lgpl.html>
 * 		GPL <http://www.gnu.org/licenses/gpl.html>
 * 		Apache License, Version 2.0 <http://www.apache.org/licenses/LICENSE-2.0.html>
 */
namespace Highcharts\Controller;

use Highcharts\Controller\AppController;

class StackedDemoController extends AppController {

        public $name = 'StackedDemo';
        public $uses = array();
        public $layout = 'Highcharts.demo';
        public $johnData = array(5, 3, 4, 7, 2);
        public $janeData = array(2, 2, 3, 2, 1);
        public $joeData = array(3, 4, 4, 2, 5);
        public $janetData = array(3, 0, 4, 4, 3);
        
        public function initialize() {
                parent::initialize();
                $this->loadComponent('Highcharts.Highcharts');
        }

        public function bar() {
                
                $chartName = 'Stacked Bar Chart';

                $myChart= $this->Highcharts->createChart();

                $myChart->chart->renderTo = "container";
                $myChart->chart->type = "bar";
                $myChart->title->text = "Stacked Bar Chart";
                $myChart->xAxis->categories = array(
                    'Apples',
                    'Oranges',
                    'Pears',
                    'Grapes',
                    'Bananas'
                );
                $myChart->yAxis->min = 0;
                $myChart->yAxis->title->text = "Total Fruit Consumption";

                $myChart->tooltip->formatter = $this->Highcharts->createJsExpr("function() {
                    return '' + this.series.name +': '+ this.y +'';}");

                $myChart->legend->backgroundColor = "#FFFFFF";
                $myChart->legend->reversed = 1;
                $myChart->plotOptions->series->stacking = "normal";

                $myChart->series[] = array(
                    'name' => "John",
                    'data' => $this->johnData
                );

                $myChart->series[] = array(
                    'name' => "Jane",
                    'data' =>  $this->janeData
                );

                $myChart->series[] = array(
                    'name' => "Joe",
                    'data' =>  $this->joeData
                );
                
                $this->set(compact('myChart', 'chartName'));
        }

        public function column() {
                
                $chartName = 'Stacked Column Chart';

                $myChart = $this->Highcharts->createChart();

                $myChart->chart->renderTo = "container";
                $myChart->chart->type = "column";
                $myChart->title->text = "Stacked Column Chart";
                $myChart->xAxis->categories = array(
                    'Apples',
                    'Oranges',
                    'Pears',
                    'Grapes',
                    'Bananas'
                );
                $myChart->yAxis->min = 0;
                $myChart->yAxis->title->text = "Total Fruit Consumption";
                $myChart->yAxis->stackLabels->enabled = 1;
                $myChart->yAxis->stackLabels->style->fontWeight = "bold";
                $myChart->yAxis->stackLabels->style->color = $this->Highcharts->createJsExpr(
                    "(Highcharts.theme && Highcharts.theme.textColor) || 'gray'");
                $myChart->legend->align = "right";
                $myChart->legend->x = - 100;
                $myChart->legend->verticalAlign = "top";
                $myChart->legend->y = 20;
                $myChart->legend->floating = 1;
                $myChart->legend->backgroundColor = $this->Highcharts->createJsExpr(
                    "(Highcharts.theme && Highcharts.theme.legendBackgroundColorSolid) || 'white'");
                $myChart->legend->borderColor = "#CCC";
                $myChart->legend->borderWidth = 1;
                $myChart->legend->shadow = false;

                $myChart->tooltip->formatter = $this->Highcharts->createJsExpr(
                    "function() {
                    return '<b>'+ this.x +'</b><br/>'+
                    this.series.name +': '+ this.y +'<br/>'+
                    'Total: '+ this.point.stackTotal;}");

                $myChart->plotOptions->column->stacking = "normal";
                $myChart->plotOptions->column->dataLabels->enabled = 1;
                $myChart->plotOptions->column->dataLabels->color = $this->Highcharts->createJsExpr(
                    "(Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white'");

                $myChart->series[] = array(
                    'name' => "John",
                    'data' => $this->johnData
                );

                $myChart->series[] = array(
                    'name' => "Jane",
                    'data' => $this->janeData
                );

                $myChart->series[] = array(
                    'name' => "Joe",
                    'data' => $this->janeData
                );
                
                $this->set(compact('myChart', 'chartName'));
        }

        public function percent_column() {

                $chartName = 'Stacked Percentage Column Chart';

                $myChart = $this->Highcharts->createChart();

                $myChart->chart->renderTo = "container";
                $myChart->chart->type = "column";
                $myChart->title->text = "Stacked Column Chart";
                $myChart->xAxis->categories = array(
                    'Apples',
                    'Oranges',
                    'Pears',
                    'Grapes',
                    'Bananas'
                );
                $myChart->yAxis->min = 0;
                $myChart->yAxis->title->text = "Total Fruit Consumption";

                $myChart->tooltip->formatter = $this->Highcharts->createJsExpr(
                    "function() {
                    return ''+ this.series.name +': '+ this.y +' ('+ Math.round(this.percentage) +'%)';}");

                $myChart->plotOptions->column->stacking = "percent";

                $myChart->series[] = array(
                    'name' => "John",
                    'data' => $this->johnData
                );

                $myChart->series[] = array(
                    'name' => "Jane",
                    'data' => $this->janeData
                );

                $myChart->series[] = array(
                    'name' => "Joe",
                    'data' => $this->joeData
                );
                
                $this->set(compact('myChart', 'chartName'));
        }

        public function grouped_column() {
                
                $chartName = 'Stacked Grouped Column Chart';
                
                $myChart = $this->Highcharts->createChart();

                $myChart->chart->renderTo = "container";
                $myChart->chart->type = "column";
                $myChart->title->text = "Total Fruit Consumption, grouped by gender";
                $myChart->xAxis->categories = array(
                    'Apples',
                    'Oranges',
                    'Pears',
                    'Grapes',
                    'Bananas'
                );
                $myChart->yAxis->allowDecimals = false;
                $myChart->yAxis->min = 0;
                $myChart->yAxis->title->text = "Number of Fruits";
                $myChart->tooltip->formatter = $this->Highcharts->createJsExpr(
                    "function() {
                    return '<b>'+ this.x +'</b><br/>'+
                    this.series.name +': '+ this.y +'<br/>'+
                    'Total: '+ this.point.stackTotal;}");

                $myChart->plotOptions->column->stacking = "normal";

                $myChart->series[] = array(
                    'name' => "John",
                    'data' => $this->johnData,
                    'stack' => 'male'
                );

                $myChart->series[] = array(
                    'name' => "Joe",
                    'data' => $this->joeData,
                    'stack' => 'male'
                );

                $myChart->series[] = array(
                    'name' => "Jane",
                    'data' => $this->janeData,
                    'stack' => 'female'
                );

                $myChart->series[] = array(
                    'name' => "Janet",
                    'data' => $this->janetData,
                    'stack' => 'female'
                );              
                
                $this->set(compact('myChart', 'chartName'));
        }

}
