<?php echo $this->Highcharts->includeExtraScripts(); ?>
<div class="chart">
        <h4>Bubble Chart</h4>
        <div id="bubblewrapper" style="display: block; float: left; width:90%; margin-bottom: 20px;"></div>
        <div class="clear"></div>
        <?php echo $this->Highcharts->render($myChart, $chartName); ?>
</div>