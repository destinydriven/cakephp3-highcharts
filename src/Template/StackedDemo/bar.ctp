<div class="chart">
        <h4>Stacked Bar Chart</h4>
        <div id="barwrapper" style="display: block; float: left; width:90%; margin-bottom: 20px;"></div>
        <div class="clear"></div>
        <?php echo $this->Highcharts->render($myChart, $chartName); ?>
</div>