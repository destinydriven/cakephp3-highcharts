<div class="chart">
        <h4>Spline Chart</h4>
        <div id="splinewrapper" style="display: block; float: left; width:90%; margin-bottom: 20px;"></div>
        <div class="clear"></div>
        <?php echo $this->Highcharts->render($myChart, $chartName); ?>
</div>