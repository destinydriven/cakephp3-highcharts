<div class="chart">
        <h4>Donut Chart</h4>
        <div id="donutwrapper" style="display: block; float: left; width:90%; margin-bottom: 20px;"></div>
        <div class="clear"></div>
        <?php echo $this->Highcharts->render($myChart, $chartName); ?>
</div>